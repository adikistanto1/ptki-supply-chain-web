<?php
	header('Content-Type: application/json');
 
    require "db_connect.php";
    $conn = mysqli_connect($dbhostname,  $dbusername, $dbpassword, $dbname);

    if (!$conn)
        die("Connection failed: " . mysqli_connect_error());

    $query = mysqli_query($conn, "SELECT a.*,IF(a.tanggal_masuk>target_masuk,'Terlambat','Ontime') as status FROM (
SELECT tpe.id_penerimaan as id_penerimaan,tpo.id_order as id_order,tp.nama_produk as nama_produk, tv.nama_vendor as nama_vendor, tu.name as nama_penerima,tpo.tanggal_target_kirim as target_masuk, tpe.tanggal_penerimaan as tanggal_masuk, tpo.id_produk_order as id_produk_order,tpo.jumlah_produk as jumlah_produk FROM tb_penerimaan tpe
JOIN tb_produk_order tpo ON tpo.id_produk_order = tpe.id_produk_order
JOIN tb_produk tp ON tp.id_produk = tpo.id_produk
JOIN tb_order tor ON tor.id_order = tpo.id_order
JOIN tb_vendor tv ON tv.id_vendor = tor.id_vendor
JOIN tb_user tu ON tu.id_user = tpe.id_user) a WHERE a.id_produk_order NOT IN (SELECT b.id_produk_order FROM tb_pengecekan b) ORDER BY id_penerimaan DESC;");
    
    if($query){
	
		$all = mysqli_fetch_all($query, MYSQLI_ASSOC);
	
		if(!empty($all)){
			
			$response = array();
			$response['success'] = true;
			$response["message"] = "Ambil data berhasil";
			
			$data = array();
			
			foreach($all as $row){
				
				$evaluasi = array();
				$evaluasi["id_order"] 		= $row["id_order"];
				$evaluasi["nama_produk"] 	= $row["nama_produk"];
				$evaluasi["nama_vendor"] 	= $row["nama_vendor"];
				$evaluasi["nama_penerima"] 	= $row["nama_penerima"];
				$evaluasi["status"] 		= $row["status"];
				$evaluasi["id_produk_order"]= $row["id_produk_order"];
				$evaluasi["jumlah_produk"] 	= $row["jumlah_produk"];
				
				array_push($data,$evaluasi);
			}
			
			$response["data"] = $data;
			
			echo json_encode($response);
			
		}else{
			
			$response = array();
			$response["success"] = false;
			$response["message"] = "Tidak ada data";
			echo json_encode($response);
			
		}
       	
    }else{
		$response = array();
		$response["success"] = false;
		$response["message"] = "Internal server error";
		echo json_encode($response);
	}

?>