<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Penerimaan extends CI_Model {
   
	public function simpan_penerimaan()
	{
            $data = array(
                'id_produk_order' => $this->input->post('nama_vendor'),
                'id_user'         => $this->input->post('telpon_vendor')
            );
            
            $this->db->insert('tb_vendor', $data);
	}
	
	public function get_list_penerimaan()
	{
            $q = "SELECT a.*,IF(a.tanggal_masuk>target_masuk,'Terlambat','Ontime') as status  FROM (
SELECT tpe.id_penerimaan as id_penerimaan,tpo.id_order as id_order,tp.nama_produk as nama_produk, tv.nama_vendor as nama_vendor, tu.name as nama_penerima,tpo.tanggal_target_kirim as target_masuk, tpe.tanggal_penerimaan as tanggal_masuk FROM tb_penerimaan tpe
JOIN tb_produk_order tpo ON tpo.id_produk_order = tpe.id_produk_order
JOIN tb_produk tp ON tp.id_produk = tpo.id_produk
JOIN tb_order tor ON tor.id_order = tpo.id_order
JOIN tb_vendor tv ON tv.id_vendor = tor.id_vendor
JOIN tb_user tu ON tu.id_user = tpe.id_user) a ORDER BY id_penerimaan DESC";
            $query = $this->db->query($q);
            return $query->result_array();
	}
}