<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Pengeluaran extends CI_Model {
   
    public function simpan_pengeluaran($is_edit)
    {
        $data = array(
            'id_produk'        => $this->input->post('id_produk'),
            'jumlah_keluar'    => $this->input->post('jumlah_produk')
        );
        
        if ($is_edit === false) {
            $this->db->insert('tb_pengeluaran', $data);
        } else {
            $id =  $this->input->post('id_pengeluaran');
            $this->db->where('id_pengeluaran', $id);
            $this->db->update('tb_pengeluaran', $data);
        }
    }

    public function get_pengeluaran()
    {
        $this->db->select('*');
        $this->db->from('tb_pengeluaran tp');
        $this->db->join('tb_produk tk', 'tk.id_produk=tp.id_produk');
        $this->db->order_by('id_pengeluaran','desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    
    public function get_pengeluaran_id($id)
    {
        $this->db->select('*');
        $this->db->where('id_pengeluaran', $id);
        $this->db->from('tb_pengeluaran');
        $query = $this->db->get();
        return $query->row_array();
    }

    public function hapus_pengeluaran($id){
        $this->db->where('id_pengeluaran', $id);
        $this->db->delete('tb_pengeluaran');
    }
    
     public function get_pemasukan()
	{
            $q = "SELECT c.id_produk, c.nama_produk as nama_produk,a.tanggal_pengecekan as waktu_masuk,SUM(b.jumlah_produk - a.jumlah_reject) as jumlah_masuk, e.nama_vendor as nama_vendor
FROM tb_pengecekan a
JOIN tb_produk_order b ON b.id_produk_order = a.id_produk_order
JOIN tb_produk c ON c.id_produk = b.id_produk 
JOIN tb_order d ON d.id_order = b.id_order 
JOIN tb_vendor e ON e.id_vendor = d.id_vendor ORDER BY a.id_pengecekan DESC";
            $query = $this->db->query($q);
            return $query->result_array();
	}
    
    
    //===================STOK=============
    public function get_stok(){
        $q ="SELECT h.id_produk,h.nama_produk,IF(SUM(h.jumlah_masuk - h.jumlah_keluar) IS NULL,0, SUM(h.jumlah_masuk - h.jumlah_keluar)) as stok,h.nama_kategori_produk FROM (
                                     
	SELECT c.id_produk, c.nama_produk,SUM(b.jumlah_produk) as jumlah_masuk, SUM(a.jumlah_reject) as jumlah_keluar,dd.nama_kategori_produk
                FROM tb_pengecekan a  
                JOIN tb_produk_order b ON b.id_produk_order = a.id_produk_order 
                RIGHT JOIN tb_produk c ON c.id_produk = b.id_produk
    			JOIN tb_kategori_produk dd ON dd.id_kategori_produk = c.id_kategori_produk
                GROUP BY c.id_produk
                
    UNION 
    
    SELECT b.id_produk, b.nama_produk, 0 as jumlah_masuk,SUM(a.jumlah_keluar) as jumlah_keluar, cc.nama_kategori_produk
    FROM tb_pengeluaran a
    RIGHT JOIN tb_produk b ON b.id_produk = a.id_produk
    JOIN tb_kategori_produk cc ON cc.id_kategori_produk = b.id_kategori_produk
    GROUP BY b.id_produk)h GROUP BY h.id_produk";
        $query = $this->db->query($q);
        return $query->result_array();
    }
    
    
    // PEMBAYARAN
    public function get_list_pembayaran()
	{
            $q = "SELECT a.* FROM (
SELECT tpe.id_pengecekan as id_pengecekan,tpo.id_order as id_order,tp.nama_produk as nama_produk, tv.nama_vendor as nama_vendor,tpo.id_produk_order as id_produk_order, tpo.jumlah_produk as jumlah_masuk, tpe.jumlah_reject as jumlah_reject, tp.harga_produk as harga_produk,((tpo.jumlah_produk - tpe.jumlah_reject)*tp.harga_produk) as total_bayar, DATE_ADD(tpe.tanggal_pengecekan, INTERVAL 1 MONTH) as waktu_bayar
FROM tb_pengecekan tpe
JOIN tb_penerimaan tme ON tme.id_produk_order = tpe.id_produk_order
JOIN tb_produk_order tpo ON tpo.id_produk_order = tpe.id_produk_order
JOIN tb_produk tp ON tp.id_produk = tpo.id_produk
JOIN tb_order tor ON tor.id_order = tpo.id_order
JOIN tb_vendor tv ON tv.id_vendor = tor.id_vendor) a ORDER BY a.id_pengecekan DESC";
            $query = $this->db->query($q);
            return $query->result_array();
	}
}