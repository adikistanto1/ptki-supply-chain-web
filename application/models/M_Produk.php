<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Produk extends CI_Model {
   
    public function simpan_produk($is_edit)
    {
        $data = array(
            'nama_produk'           => $this->input->post('nama_produk'),
            'spesifikasi_produk'    => $this->input->post('spesifikasi_produk'),
            'id_kategori_produk'    => $this->input->post('id_kategori_produk'),
            'harga_produk'          => $this->input->post('harga_produk'),
            'satuan_produk'         => $this->input->post('satuan_produk')
        );
        
        if ($is_edit === false) {
            $this->db->insert('tb_produk', $data);
        } else {
            $id =  $this->input->post('id_produk');
            $this->db->where('id_produk', $id);
            $this->db->update('tb_produk', $data);
        }
    }

    public function get_produk()
    {
        $this->db->select('*');
        $this->db->from('tb_produk to');
        $this->db->join('tb_kategori_produk tk', 'tk.id_kategori_produk=to.id_kategori_produk', 'left');
        $this->db->order_by('id_produk','desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    
    public function get_produk_id($id)
    {
        $this->db->select('*');
        $this->db->where('id_produk', $id);
        $this->db->from('tb_produk');
        $query = $this->db->get();
        return $query->row_array();
    }


    public function get_total_produk()
    {
        $this->db->select('*');
        $this->db->from('tb_produk');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function hapus_produk($id){
        $this->db->where('id_produk', $id);
        $this->db->delete('tb_produk');
    }
    
    
    //===================STOK=============
    public function check_stok_item($id_produk){
        $q = "SELECT a.id_produk,a.nama_produk as nama_produk ,IF(SUM(a.jumlah) IS NULL,0,SUM(a.jumlah)) as stok FROM(
    
                SELECT tb_produk.id_produk as id_produk,SUM(jumlah_return) as jumlah,tb_produk.nama_produk as nama_produk 
                FROM tb_produk_return  
                RIGHT JOIN tb_produk 
                ON tb_produk.id_produk = tb_produk_return.id_produk
                JOIN tb_return
                ON tb_return.id_return = tb_produk_return.id_return
                WHERE tb_return.jenis_return != '1' /*rusak*/
                AND tb_produk.id_produk='$id_produk'

                UNION

                SELECT tb_produk.id_produk as id_produk,SUM(jumlah_sukses) as jumlah,tb_produk.nama_produk as nama_produk 
                FROM tb_alokasi
                JOIN tb_produk_order 
                ON tb_produk_order.id_produk_order=tb_alokasi.id_produk_order
                RIGHT JOIN tb_produk 
                ON tb_produk.id_produk = tb_produk_order.id_produk
                WHERE tb_produk.id_produk='$id_produk' 

                UNION

                SELECT tb_produk.id_produk as id_produk, SUM(jumlah_produk * -1) as jumlah,tb_produk.nama_produk as nama_produk 
                FROM tb_produk_order 
                RIGHT JOIN tb_produk 
                ON tb_produk.id_produk = tb_produk_order.id_produk
                WHERE tb_produk.id_produk='$id_produk'
                    
                UNION
                
                SELECT tb_produk.id_produk as id_produk,SUM(jumlah_produk) as jumlah,tb_produk.nama_produk as nama_produk
                FROM tb_belanja_produk  
                RIGHT JOIN tb_produk 
                ON tb_produk.id_produk = tb_belanja_produk.id_produk
                WHERE tb_produk.id_produk='$id_produk'

            ) a";
        $query = $this->db->query($q);
        return $query->row_array();
    }
    
    public function check_stok_all(){
        $q ="SELECT a.id_produk,a.nama_produk as nama_produk,IF(SUM(a.jumlah) IS NULL,0,SUM(a.jumlah)) as stok, a.spesifikasi_produk as spesifikasi_produk, a.barcode_produk as barcode_produk, a.satuan as satuan FROM(
    
                SELECT tb_produk.id_produk as id_produk,SUM(jumlah_return) as jumlah,tb_produk.nama_produk as nama_produk, tb_produk.spesifikasi_produk as spesifikasi_produk, tb_produk.barcode_produk as barcode_produk, tb_produk.satuan as satuan
                FROM tb_produk_return  
                RIGHT JOIN tb_produk 
                ON tb_produk.id_produk = tb_produk_return.id_produk
                JOIN tb_return
                ON tb_return.id_return = tb_produk_return.id_return
                WHERE tb_return.jenis_return != '1' /*rusak*/
                GROUP BY tb_produk.id_produk

                UNION

                SELECT tb_produk.id_produk as id_produk,SUM(jumlah_sukses) as jumlah,tb_produk.nama_produk as nama_produk, tb_produk.spesifikasi_produk as spesifikasi_produk, tb_produk.barcode_produk as barcode_produk, tb_produk.satuan as satuan 
                FROM tb_alokasi
                JOIN tb_produk_order 
                ON tb_produk_order.id_produk_order=tb_alokasi.id_produk_order
                RIGHT JOIN tb_produk 
                ON tb_produk.id_produk = tb_produk_order.id_produk
                GROUP BY tb_produk.id_produk 

                UNION

                SELECT tb_produk.id_produk as id_produk, SUM(jumlah_produk * -1) as jumlah,tb_produk.nama_produk as nama_produk, tb_produk.spesifikasi_produk as spesifikasi_produk, tb_produk.barcode_produk as barcode_produk, tb_produk.satuan as satuan 
                FROM tb_produk_order 
                RIGHT JOIN tb_produk 
                ON tb_produk.id_produk = tb_produk_order.id_produk
                GROUP BY tb_produk.id_produk
                
                UNION
                
                SELECT tb_produk.id_produk as id_produk,SUM(jumlah_produk) as jumlah,tb_produk.nama_produk as nama_produk, tb_produk.spesifikasi_produk as spesifikasi_produk, tb_produk.barcode_produk as barcode_produk, tb_produk.satuan as satuan  
                FROM tb_belanja_produk  
                RIGHT JOIN tb_produk 
                ON tb_produk.id_produk = tb_belanja_produk.id_produk
                GROUP BY tb_produk.id_produk

            ) a GROUP BY a.id_produk";
        $query = $this->db->query($q);
        return $query->result_array();
    }
}