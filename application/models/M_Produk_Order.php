<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Produk_Order extends CI_Model {
   
    public function simpan_produk_order($id_produk,$harga_produk)
    {
        $data = array(
            'id_order'              => $this->input->post('id_order'),
            'id_produk'             => $id_produk,
            'jumlah_produk'         => $this->input->post('jumlah_produk'),
            'tanggal_target_kirim'  => $this->input->post('tanggal_target_kirim'),
            'harga_produk'          => $harga_produk
        );
        
        $this->db->insert('tb_produk_order', $data);
    }
    
    public function update_produk_order($jml_sebelumnya,$id_produk,$harga_produk)
    {   
        $jml_baru = $this->input->post('jumlah_produk');
        $jml_total= $jml_sebelumnya + $jml_baru;
        
        $data = array(
            'id_produk'             => $id_produk,
            'id_order'              => $this->input->post('id_order'),
            'harga_produk'        => $harga_produk,
            'tanggal_target_kirim'  => $this->input->post('tanggal_target_kirim'),
            'jumlah_produk'         => $jml_total         
        );
       
        
        $id_order = $this->input->post('id_order');
        $this->db->where('id_order', $id_order);
        $this->db->where('id_produk', $id_produk);
        $this->db->update('tb_produk_order', $data);     
    }

    public function get_produk_order($id)
    {
        $this->db->select('*');
        $this->db->from('tb_produk_order tpo');
        $this->db->join('tb_produk tp', 'tp.id_produk=tpo.id_produk', 'left');
        $this->db->where('tpo.id_order', $id);
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function get_produk_order_id($id)
    {
        $this->db->select('*');
        $this->db->from('tb_produk_order tpo');
        $this->db->join('tb_produk tp', 'tp.id_produk=tpo.id_produk');
        $this->db->where('tpo.id_produk_order', $id);
        $query = $this->db->get();
        return $query->result_array();
    }
    
     public function cek_order_produk($id_order,$id_produk)
    {
        $this->db->select('*');
        $this->db->from('tb_produk_order to');
        $this->db->where('to.id_order', $id_order);
        $this->db->where('to.id_produk', $id_produk);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function get_total_order()
    {
        $this->db->select('*');
        $this->db->from('tb_produk_order');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function hapus_produk_order($id){
        $this->db->where('id_produk_order', $id);
        $this->db->delete('tb_produk_order');
    }
    
}