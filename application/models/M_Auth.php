<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Auth extends CI_Model {
   
	public function check_user($username,$password)
	{
		$query = $this->db->get_where('tb_user', array('username' => $username, 'password' => $password));
		return $query->row();
	}
	
        public function get_user($username,$password){
            $this->db->select('*');
            $this->db->from('tb_user tu');
            $this->db->join('tb_role tr', 'tr.id_role=tu.role', 'left');
            $this->db->where('username',$username);
            $this->db->where('password',$password);
            $query = $this->db->get();
            return $query->row_array();
        }
        
        
}