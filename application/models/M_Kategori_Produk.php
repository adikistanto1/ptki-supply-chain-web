<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Kategori_Produk extends CI_Model {
   
	public function simpan($id)
	{
            $data = array(
                'name_kategori_produk'          => $this->input->post('nama_kategori'),
                'keterangan_kategori_produk'    => $this->input->post('keterangan_kategori')
            );
            if ($id === false) {
                $this->db->insert('tb_kategori_produk', $data);
            } else {
                $this->db->where('id_kategori_produk', $id);
                $this->db->update('tb_kategori_produk', $data);
            }
	}
	
	public function get_kategori_produk()
	{
            $this->db->select('*');
            $this->db->from('tb_kategori_produk');
            $query = $this->db->get();
            return $query->result_array();
	}
        
        public function get_kategori_produk_id($id)
	{
            $this->db->select('*');
            $this->db->from('tb_kategori_produk');
            $this->db->where('id_kategori_produk', $id);
            $query = $this->db->get();
            return $query->result_array();
	}
        
        public function hapus_kategori_produk($id){
            $this->db->where('id_kategori_produk', $id);
            $this->db->delete('tb_kategori_produk');
        }
}