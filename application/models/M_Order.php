<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Order extends CI_Model {
   
    public function simpan_order($is_edit)
    {
        $data = array(
            'id_vendor'     => $this->input->post('id_vendor'),
            'keterangan'    => $this->input->post('keterangan'),
            'status_order'  => $this->input->post('status_order')
        );
        
        if ($is_edit === false) {
            $this->db->insert('tb_order', $data);
        } else {
            $id =  $this->input->post('id_order');
            $this->db->where('id_order', $id);
            $this->db->update('tb_order', $data);
        }
    }

    public function get_order()
    {
        $this->db->select('*');
        $this->db->from('tb_order to');
        $this->db->join('tb_vendor tv', 'tv.id_vendor=to.id_vendor', 'left');
        $this->db->order_by('id_order','desc');
        $query = $this->db->get();
        return $query->result_array();
    }
    
     public function get_order_vendor($id_vendor)
    {
        $this->db->select('*');
        $this->db->from('tb_order to');
        $this->db->join('tb_vendor tv', 'tv.id_vendor=to.id_vendor', 'left');
        $this->db->where('to.id_vendor',$id_vendor);
        $this->db->order_by('id_order','desc');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function get_order_count_product()
    {
        $this->db->select('ta.id_order,count(ta.id_order) as count_produk');
        $this->db->from('tb_produk_order ta');
        $this->db->group_by('ta.id_order');
        $query = $this->db->get();
        return $query->result_array();
    }

    
    public function get_order_id($id)
    {
        $this->db->select('*');
        $this->db->from('tb_order to');
        $this->db->join('tb_vendor tv', 'tv.id_vendor=to.id_vendor');
        $this->db->where('to.id_order', $id);
        $query = $this->db->get();
        return $query->row_array();
    }
    
    public function ubah_status_order($status,$id)
    {
        $data = array(
            'status_order' => $status
        );
        
        $this->db->where('id_order', $id);
        $this->db->update('tb_order', $data);
    }


    public function get_total_order()
    {
        $this->db->select('*');
        $this->db->from('tb_order');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function hapus_order($id){
        $this->db->where('id_order', $id);
        $this->db->delete('tb_order');
    }
    
}