<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Vendor extends CI_Model {
   
	public function simpan_vendor($is_edit)
	{
            $data = array(
                'nama_vendor'       => $this->input->post('nama_vendor'),
                'telpon_vendor'     => $this->input->post('telpon_vendor'),
                'alamat_vendor'     => $this->input->post('alamat_vendor')
            );
            if ($is_edit === false) {
                $this->db->insert('tb_vendor', $data);
            } else {
                $id = $this->input->post('id_vendor');
                $this->db->where('id_vendor', $id);
                $this->db->update('tb_vendor', $data);
            }
	}
	
	public function get_list_vendor()
	{
            $this->db->select('*');
            $this->db->from('tb_vendor');
            $this->db->order_by('id_vendor','desc');
            $query = $this->db->get();
            return $query->result_array();
	}
        
        public function get_vendor_id($id)
	{
            $this->db->select('*');
            $this->db->from('tb_vendor');
            $this->db->where('id_vendor', $id);
            $query = $this->db->get();
            return $query->row_array();
	}
        
        
        public function hapus_vendor($id){
            $this->db->where('id_vendor', $id);
            $this->db->delete('tb_vendor');
        }
}