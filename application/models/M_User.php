<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_User extends CI_Model {
   
	public function simpan_akun($id)
	{
            $data = array(
                'name'      => $this->input->post('name'),
                'username'  => $this->input->post('username'),
                'password'  => $this->input->post('password'),
                'role'      => $this->input->post('role')
            );
            if ($id === false) {
                $this->db->insert('tb_user', $data);
            } else {
                $this->db->where('id_user', $id);
                $this->db->update('tb_user', $data);
            }
	}
        
        public function simpan_akun1($id)
	{
            $data = array(
                'name'      => $this->input->post('name'),
                'username'  => $this->input->post('username'),
                'password'  => $this->input->post('password'),
                'role'      => $this->input->post('role'),
                'id_vendor'      => $this->input->post('id_vendor')
            );
            if ($id === false) {
                $this->db->insert('tb_user', $data);
            } else {
                $this->db->where('id_user', $id);
                $this->db->update('tb_user', $data);
            }
	}
	
	public function get_list_akun($role_code)
	{
            $this->db->select('*');
            $this->db->from('tb_user');
            $this->db->where('role', $role_code);
            $query = $this->db->get();
            return $query->result_array();
	}
        
        public function get_list_akun_vendor()
	{
            $this->db->select('*');
            $this->db->from('tb_user tu');
            $this->db->join('tb_vendor tv', 'tv.id_vendor=tu.id_vendor');
            $query = $this->db->get();
            return $query->result_array();
	}
        
        public function hapus_akun($id){
            $this->db->where('id_user', $id);
            $this->db->delete('tb_user');
        }
}