<?php

class MY_Loader extends CI_Loader{
    
    public function append($text, $return = false) {
        $this->output->append_output($text);
        if ($return)
            return $text;
    }
    
    public function viewAdmin($page_name, $vars = array(), $return = FALSE) {
        //$content = $this->view('administrasi/header', $vars, $return);
        $content = $this->view('admin/header', $vars, $return);
        $content = $this->view($page_name, $vars, $return);
        $content = $this->view('admin/footer', $vars, $return);

        if ($return) {
            return $content;
        }
    }
    
    public function viewPurchase($page_name, $vars = array(), $return = FALSE) {
        $content = $this->view('purchase/header', $vars, $return);
        //$content = $this->view('administrasi/header', $vars, $return);
        $content = $this->view($page_name, $vars, $return);
        $content = $this->view('purchase/footer', $vars, $return);

        if ($return) {
            return $content;
        }
    }
    
    public function viewVendor($page_name, $vars = array(), $return = FALSE) {
        //$content = $this->view('produksi/header', $vars, $return);
        $content = $this->view('vendors/header', $vars, $return);
        $content = $this->view($page_name, $vars, $return);
        $content = $this->view('vendors/footer', $vars, $return);

        if ($return) {
            return $content;
        }
    }
    
    public function viewPenerima($page_name, $vars = array(), $return = FALSE) {
        $content = $this->view('penerima/header', $vars, $return);
        //$content = $this->view('sales/header', $vars, $return);
        $content = $this->view($page_name, $vars, $return);
        $content = $this->view('penerima/footer', $vars, $return);

        if ($return) {
            return $content;
        }
    }
    
    public function viewQC($page_name, $vars = array(), $return = FALSE) {
        $content = $this->view('qc/header', $vars, $return);
        //$content = $this->view('sales/header', $vars, $return);
        $content = $this->view($page_name, $vars, $return);
        $content = $this->view('qc/footer', $vars, $return);

        if ($return) {
            return $content;
        }
    }
    
    public function viewGudang($page_name, $vars = array(), $return = FALSE) {
        $content = $this->view('gudang/header', $vars, $return);
        //$content = $this->view('sales/header', $vars, $return);
        $content = $this->view($page_name, $vars, $return);
        $content = $this->view('gudang/footer', $vars, $return);

        if ($return) {
            return $content;
        }
    }
}

