<section class="content-header">
    <h1 style="margin-bottom: 15px">
        Welcome...
    </h1>
    <div class="panel panel-default">
        <div class="panel-body">
           
          <div class="row">
            <div class="col-sm-6">
              <div class="card">
                <div class="card-body">
                  <h3 class="card-title">Features</h3>
                  <p class="card-text">
                     1. Barang yang akan mucul di menu barang diterima. Dan jika sudah dicek maka akan hilang dari list. Selanjutnya akan muncul di menu barang dicek.<br>
                     2. Di web hanya menampilkan list, pengecekan dilakukan menggunakan aplikasi android kusus QC, sehingga lebih praktis untuk melakukan pengecekan.<br>
                     3. Jumlah reject akan otomatis menghitung total bayar, yang muncul di bagian Purchase menu pembayaran.<br>
                     4. Terdapat fitur cetak listing, mestinya bisa difilter by date list yang ingin dicetak
                  </p>
                  <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
</section>





