<section class="content-header">
    <h1 style="margin-bottom: 15px">
        Daftar Barang Diterima
    </h1>

    <?php
        $message = $this->session->flashdata('message');
        if (isset($error)) {
            echo"<div class='alert alert-warning' role='alert'>" . $error . "<button type='button' class='close' data-dismiss='alert'' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button></div>";
        } else if(isset ($message)) {
            echo"<div class='alert alert-success' role='alert'>" . $message . "<button type='button' class='close' data-dismiss='alert'' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button></div>";
        }
    ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <table id="table" class="table table-hover">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nomor Order</th>
                    <th>Produk</th>
                    <th>Vendor</th>
                    <th>Target Masuk</th>
                    <th>Tanggal Masuk</th>
                    <th>Status</th>
                    <th>Penerima</th>
                </tr>
                </thead>
                <tbody>
                <?php if(empty($produkorder)): ?>
                <!--<tr>-->
                <!--    <td colspan="7" class="text-center">Data tidak ditemukan</td>-->
                <!--</tr>-->
                <?php else: 
                    $key = 0;?>
                    <?php foreach($produkorder as $item): 
                        
                        if(($item['status_order']!='0')&&($item['status_order']!='1')){
                        
                        $key = $key+1?>
                    <tr>
                        <td><?php echo $key; ?></td>
                        <td>
                            <?php echo 'PO-'.$item['id_order']; ?>
                        </td>
                        <td>
                            <?php echo $item['nama_produk']; ?>
                        </td>
                        <td>
                            <?php echo $item['nama_vendor']; ?>
                        </td>
                        <td>
                            <?php echo $item['target_masuk']; ?>
                        </td>
                         <td>
                            <?php echo $item['tanggal_masuk']; ?>
                        </td>
                         <td>
                             <?php echo strtoupper($item['status']); ?>
                        </td>
                        <td>
                            <?php echo $item['nama_penerima']; ?>
                        </td>
                    </tr>
                        <?php }endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
            <a style="margin-bottom: 15px" target="_blank"class="btn btn-info uppercase" href="<?php echo base_url(); ?>control/produkterimacetak">Cetak Penerimaan Barang</a>
        </div>
    </div>
</section>
<script>
    $(function () {
	$("#table").DataTable();
    });
</script>