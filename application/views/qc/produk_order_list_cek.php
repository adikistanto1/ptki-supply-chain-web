<section class="content-header">
    <h1 style="margin-bottom: 15px">
        Daftar Barang Dicek
    </h1>

    <?php
        $message = $this->session->flashdata('message');
        if (isset($error)) {
            echo"<div class='alert alert-warning' role='alert'>" . $error . "<button type='button' class='close' data-dismiss='alert'' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button></div>";
        } else if(isset ($message)) {
            echo"<div class='alert alert-success' role='alert'>" . $message . "<button type='button' class='close' data-dismiss='alert'' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button></div>";
        }
    ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <table id="table" class="table table-hover">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nomor Order</th>
                    <th>Produk</th>
                    <th>Vendor</th>
                    <th class="text-center">Target Masuk</th>
                    <th class="text-center">Tanggal Pengecekan</th>
                    <th class="text-center">Jumlah Reject</th>
                    <th class="text-center">Pengecek</th>
                    <th>Keterangan</th>
                </tr>
                </thead>
                <tbody>
                <?php if(empty($produkcek)): ?>
                <!--<tr>-->
                <!--    <td colspan="7" class="text-center">Data tidak ditemukan</td>-->
                <!--</tr>-->
                <?php else: 
                    $key = 0;?>
                    <?php foreach($produkcek as $item): 
                        
                        if(($item['status_order']!='0')&&($item['status_order']!='1')){
                        
                        $key = $key+1?>
                    <tr>
                        <td><?php echo $key; ?></td>
                        <td>
                            <?php echo 'PO-'.$item['id_order']; ?>
                        </td>
                        <td>
                            <?php echo $item['nama_produk']; ?>
                        </td>
                        <td>
                            <?php echo $item['nama_vendor']; ?>
                        </td>
                        <td class="text-center">
                            <?php echo $item['tanggal_masuk']; ?>
                        </td>
                         <td class="text-center">
                            <?php echo $item['tanggal_pengecekan']; ?>
                        </td>
                        <td class="text-center">
                            <?php echo $item['jumlah_reject']; ?>
                        </td>
                         <td class="text-center">
                             <?php echo $item['nama_pengecek']; ?>
                        </td>
                        <td>
                            <?php echo $item['keterangan']; ?>
                        </td>
                    </tr>
                        <?php }endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
            <a style="margin-bottom: 15px" target="_blank"class="btn btn-info uppercase" href="<?php echo base_url(); ?>control/produkcekcetak">Cetak Pemeriksaan Barang</a>
        </div>
    </div>
</section>
<script>
    $(function () {
	$("#table").DataTable();
    });
</script>