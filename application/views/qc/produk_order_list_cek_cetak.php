<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/bootstrap/css/bootstrap.min.css">
<style>
th, td {
    padding: 5px;
}
</style>
<section class="content-header">
    <h1 style="margin-bottom: 15px">
        Daftar Barang Dicek
    </h1>

 
    <div class="panel panel-default">
        <div class="panel-body">
            <table id="table" class="table table-bordered">
                <thead>
                <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">Nomor Order</th>
                    <th class="text-center">Produk</th>
                    <th class="text-center">Vendor</th>
                    <th class="text-center">Target Masuk</th>
                    <th class="text-center">Tanggal Pengecekan</th>
                    <th class="text-center">Jumlah Reject</th>
                    <th class="text-center">Pengecek</th>
                    <th>Keterangan</th>
                </tr>
                </thead>
                <tbody>
                <?php if(empty($produkcek)): ?>
                <!--<tr>-->
                <!--    <td colspan="7" class="text-center">Data tidak ditemukan</td>-->
                <!--</tr>-->
                <?php else: 
                    $key = 0;?>
                    <?php foreach($produkcek as $item): 
                        
                        if(($item['status_order']!='0')&&($item['status_order']!='1')){
                        
                        $key = $key+1?>
                    <tr>
                        <td class="text-center"><?php echo $key; ?></td>
                        <td class="text-center">
                            <?php echo 'PO-'.$item['id_order']; ?>
                        </td>
                        <td class="text-center">
                            <?php echo $item['nama_produk']; ?>
                        </td>
                        <td class="text-center">
                            <?php echo $item['nama_vendor']; ?>
                        </td>
                        <td class="text-center">
                            <?php echo $item['tanggal_masuk']; ?>
                        </td>
                         <td class="text-center">
                            <?php echo $item['tanggal_pengecekan']; ?>
                        </td>
                        <td class="text-center">
                            <?php echo $item['jumlah_reject']; ?>
                        </td>
                         <td class="text-center">
                             <?php echo $item['nama_pengecek']; ?>
                        </td>
                        <td class="text-center">
                            <?php echo $item['keterangan']; ?>
                        </td>
                    </tr>
                        <?php }endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        
        </div>
    </div>
</section>
<script>
    $(function () {
	$("#table").DataTable();
    });
</script>