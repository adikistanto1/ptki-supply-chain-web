<section class="content-header">
    <h1 style="margin-bottom: 15px">
        Welcome...
    </h1>
    <div class="panel panel-default">
        <div class="panel-body">
           
          <div class="row">
            <div class="col-sm-6">
              <div class="card">
                <div class="card-body">
                  <h3 class="card-title">Features</h3>
                  <p class="card-text">
                      Admin adalah satu salah karakteristik pengguna yang berperan dalam proses pembuatan akun-akun karakteristik pengguna lainnya.<br>
                      Meliputi :<br>
                      - Purchase<br>
                      - Penerima<br>
                      - QC<br>
                      - Gudang<br>
                      - Vendor<br><br>
                      
                      Setiap karakter mempunyai tugas berbeda - beda, dan mempunyai halaman aplikasi yang berbeda pula yang dapat diakses setelah melakukan login.<br> 
                      Sehingga akan mengurangi resiko kesalahan akses oleh bagian yang tidak berwenang.
                  </p>
                  <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
</section>


