<section class="content-header">
    <h1 style="margin-bottom: 15px">
        Tambah Akun <?php echo strtoupper($role_name);?>
    </h1>
    
    <div class="panel panel-default">
        <div class="panel-body">
            <form method="post" action="<?php echo base_url(); ?>admin/akunsimpan/<?php echo $role_name;?>">
                <!-- VENDOR ONLY -->
                <?php if($role_code==6):?>
                <div class="row">
                    <div class="col-lg-4 form-group">
                        <label for="email">Vendor *:</label>
                        <select class="js-example-basic-single form-control" name="id_vendor" required="">
                            <option value="">Pilih Vendor</option>
                            <?php foreach($vendor as $item):?>

                                <option value="<?php echo $item['id_vendor'];?>"><?php echo $item['nama_vendor']?></option>

                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <?php endif;?>
                <div class="row">
                    <div class="col-lg-4 form-group">
                          <label for="email">Nama:</label>
                          <input type="text" class="form-control" name="name" placeholder="Masukkan nama" required>
                          <?php echo form_error('name'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 form-group">
                          <label for="email">Username:</label>
                          <input type="text" class="form-control" name="username" placeholder="Masukkan username" required>
                          <?php echo form_error('username'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 form-group">
                          <label for="pwd">Password:</label>
                          <input type="text" class="form-control" name="password" placeholder="Masukkan password" required>
                          <?php echo form_error('password'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 form-group">
                        <input type="hidden" name="role" value="<?php echo $role_code;?>"/>
                        <button type="submit" class="btn btn-default">Submit</button>
                        <button onclick="goBack()" class="btn btn-default">Kembali</button>
                    </div>
                </div>
            </form> 
        </div>
    </div>
</section>
<script>
    function goBack() {
        window.history.back();
    }
    
     $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
</script>