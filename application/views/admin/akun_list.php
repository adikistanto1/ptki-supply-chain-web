<section class="content-header">
    <h1 style="margin-bottom: 15px">
        Daftar Akun <?php echo strtoupper($role_name);?>
    </h1>
    
    <a style="margin-bottom: 15px" class="btn btn-info" href="<?php echo base_url(); ?>admin/akunform/<?php echo $role_name?>">Tambah</a>

    <?php
        $message = $this->session->flashdata('message');
        if (isset($error)) {
            echo"<div class='alert alert-warning' role='alert'>" . $error . "</div>";
        } else if(isset ($message)) {
            echo"<div class='alert alert-success' role='alert'>" . $message . "</div>";
        }
    ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <table id="table" class="table table-hover">
                <thead>
                <tr>
                    <th>No</th>
                    <?php if($role_code==6):?>
                    <th>Vendor</th>
                    <?php endif;?>
                    <th>Nama</th>
                    <th>Username</th>
                    <th>Action</th>
                </tr>
                </thead>
                <?php if(empty($list)): ?>
<!--                <tbody>
                <tr>
                    <td colspan="4" class="text-center">Data tidak ditemukan</td>
                </tr>
                </tbody>
                <tbody>-->
                <?php else: 
                    $key = 0;?>
                    <?php foreach($list as $item): 
                        $key = $key+1?>
                    <tr>
                        <td><?php echo $key; ?></td>
                        <?php if($role_code==6):?>
                        <td><?php echo $item['nama_vendor']; ?></td>
                        <?php endif;?>
                        <td>
                            <?php echo $item['name']; ?>
                        </td>
                        <td>
                            <?php echo $item['username']; ?>
                        </td>
                        <td width="60px">
                            <form onsubmit="return confirmHapus(this);"id="myForm" method="POST" action="<?php echo base_url(); ?>admin/akunhapus/<?php echo $role_name?>">
                                <input name="id" type="hidden" value="<?php echo $item['id_user']?>"/>
                                <input type="submit" class="btn btn-danger" value="Hapus"/>
                            </form>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
                <?php endif; ?>
            </table>
        </div>
    </div>
</section>
<script>
    function confirmHapus(form){
         return confirm('Anda yakin akan menghapus data ini ?');
    }
    
    $(function () {
	$("#table").DataTable();
    });
</script>