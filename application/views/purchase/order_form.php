<section class="content-header">
    <h1 style="margin-bottom: 15px">
        Buat Order
    </h1>
    
    <?php
        $message = $this->session->flashdata('message');
        $error1 = $this->session->flashdata('error');
        if (isset($error)) {
            echo"<div class='alert alert-warning' role='alert'>" . $error . "</div>";
        } else if(isset ($message)) {
            echo"<div class='alert alert-success' role='alert'>" . $message . "</div>";
            $this->session->unset_userdata('message');
        }else if(isset ($error1)) {
            echo"<div class='alert alert-warning' role='alert'>" . $error1 . "</div>";
            $this->session->unset_userdata('error');
        }
    ?>
    
    <div class="panel panel-default">
        <div class="panel-body">
            <form method="post" action="<?php echo base_url(); ?>purchase/ordersimpan">
                 <div class="col-lg-4 form-group">
                    <label for="email">Vendor *:</label>
                    <select class="js-example-basic-single form-control" name="id_vendor" required="">
                        <option value="">Pilih Vendor</option>
                        <?php foreach($vendor as $item):?>

                            <option value="<?php echo $item['id_vendor'];?>"><?php echo $item['nama_vendor']?></option>

                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-lg-4"></div>
                
                <div class="col-lg-12 form-group pull-left">
                      <label for="email">Keterangan order:</label>
                      <textarea type="text" class="form-control" name="keterangan" placeholder="Masukkan keterangan"></textarea>
                </div>
               
                <div class="col-lg-12">
                    <input type="hidden" name="status_order" value="0"/>
                    <button type="submit" class="btn btn-default">Submit</button>
                    <button type="button" onclick="goBack()" class="btn btn-default">Kembali</button>
                </div>
            </form> 
        </div>
    </div>
</section>
<script>
    function goBack() {
        window.history.back();
    }
    
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
 
</script>