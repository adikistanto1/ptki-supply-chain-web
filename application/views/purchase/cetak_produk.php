<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/bootstrap/css/bootstrap.min.css">
<style>
th, td {
    padding: 5px;
}
</style>
<section class="content-header">
    <h1 style="margin-bottom: 15px">
        Daftar Produk
    </h1>
  
    <div class="panel panel-default">
        <div class="panel-body">
            <table id="table" class="table table-bordered">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Produk</th>
                    <th>Kategori</th>
                    <th>Spesifikasi</th>
                    <th>Satuan</th>
                    <th>Harga (Rp)</th>
                </tr>
                </thead>
                <tbody>
                <?php if(empty($produk)): ?>
                <!--<tr>-->
                <!--    <td colspan="7" class="text-center">Data tidak ditemukan</td>-->
                <!--</tr>-->
                <?php else: 
                    $key = 0;?>
                    <?php foreach($produk as $item): 
                        $key = $key+1?>
                    <tr>
                        <td><?php echo $key; ?></td>
                        <td>
                            <?php echo $item['nama_produk']; ?>
                        </td>
                        <td>
                            <?php echo $item['nama_kategori_produk']; ?>
                        </td>
                        <td>
                            <?php echo $item['spesifikasi_produk']; ?>
                        </td>
                         <td>
                            <?php echo $item['satuan_produk']; ?>
                        </td>
                        <td>
                            <?php echo number_format($item['harga_produk'] , 2 , ',','.' ); ?>
                        </td>
                       
                    </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>        
        </div>
    </div>
</section>
<script>
    $(function () {
	$("#table").DataTable();
    });
    
    function confirmHapus(form){
         return confirm('Anda yakin akan menghapus data ini ?');
    }
</script>