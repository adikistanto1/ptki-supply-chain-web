<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/bootstrap/css/bootstrap.min.css">
<style>
th, td {
    padding: 5px;
}
</style>
<section class="content-header">
    <h1 style="margin-bottom: 15px">
        Daftar Pembayaran Order
    </h1>

    <div class="panel panel-default">
        <div class="panel-body">
            <table id="table" class="table table-bordered">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nomor Order</th>
                    <th>Produk</th>
                    <th>Vendor</th>
                    <th class="text-center">Jumlah Masuk</th>
                    <th class="text-center">Jumlah Reject</th>
                    <th class="text-center">Harga</th>
                    <th class="text-center">Total Bayar</th>
                    <th class="text-center">Waktu Bayar</th>
                </tr>
                </thead>
                <tbody>
                <?php if(empty($pembayaran)): ?>
                <!--<tr>-->
                <!--    <td colspan="7" class="text-center">Data tidak ditemukan</td>-->
                <!--</tr>-->
                <?php else: 
                    $key = 0;?>
                    <?php foreach($pembayaran as $item): 
                        
                        if(($item['status_order']!='0')&&($item['status_order']!='1')){
                        
                        $key = $key+1?>
                    <tr>
                        <td><?php echo $key; ?></td>
                        <td>
                            <?php echo 'PO-'.$item['id_order']; ?>
                        </td>
                        <td>
                            <?php echo $item['nama_produk']; ?>
                        </td>
                        <td>
                            <?php echo $item['nama_vendor']; ?>
                        </td>
                        <td class="text-center">
                            <?php echo $item['jumlah_masuk']; ?>
                        </td>
                        <td class="text-center">
                            <?php echo $item['jumlah_reject']; ?>
                        </td>
                         <td class="text-center">
                             <?php echo number_format($item['harga_produk'] , 2 , ',','.' ); ?>
                        </td>
                        <td class="text-center">
                            <?php echo number_format($item['total_bayar'] , 2 , ',','.' ); ?>
                        </td>
                        <td class="text-center">
                            <?php echo date("d/m/y", strtotime($item['waktu_bayar'])); ?>
                        </td>
                    </tr>
                        <?php }endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>   
        </div>
    </div>
</section>
<script>
    $(function () {
	$("#table").DataTable();
    });
</script>