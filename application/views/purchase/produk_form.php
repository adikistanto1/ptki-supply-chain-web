<section class="content-header">
    <h1 style="margin-bottom: 15px">
        Tambah Produk
    </h1>
    
    <?php
        $message = $this->session->flashdata('message');
        $error1 = $this->session->flashdata('error');
        if (isset($error)) {
            echo"<div class='alert alert-warning' role='alert'>" . $error . "</div>";
        } else if(isset ($message)) {
            echo"<div class='alert alert-success' role='alert'>" . $message . "</div>";
            $this->session->unset_userdata('message');
        }else if(isset ($error1)) {
            echo"<div class='alert alert-warning' role='alert'>" . $error1 . "</div>";
            $this->session->unset_userdata('error');
        }
    ?>
    
    <div class="panel panel-default">
        <div class="panel-body">
            <form method="post" action="<?php echo base_url(); ?>purchase/produksimpan">
                <div class="col-lg-4 form-group">
                      <label for="email">Nama Produk *:</label>
                      <input type="text" class="form-control" name="nama_produk" placeholder="Masukkan nama produk" required>
                      <?php echo form_error('nama_produk'); ?>
                </div>
                
                 <div class="col-lg-4 form-group">
                    <label for="email">Kategori *:</label>
                    <select class="js-example-basic-single form-control" name="id_kategori_produk" required="">
                         <option value="">Pilih Kategori</option>
                      <?php foreach($kategori as $item):?>

                         <option value="<?php echo $item['id_kategori_produk'];?>"><?php echo $item['nama_kategori_produk']?></option>

                      <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-lg-4"></div>
                
                <div class="col-lg-4 form-group">
                      <label for="pwd">Harga Produk *:</label>
                      <input type="number" class="form-control" name="harga_produk" placeholder="Masukkan harga produk" required>
                      <?php echo form_error('harga_produk'); ?>
                </div>
                
                <div class="col-lg-4 form-group">
                      <label for="pwd">Satuan *:</label>
                      <select class="js-example-basic-single form-control" name="satuan_produk" required="">
                         <option value="">Pilih Satuan</option>
                         <option value="Pack">Pack</option>
                         <option value="Dus">Dus</option>
                         <option value="Item">Item</option>
                    </select>
                </div>
                
                <div class="col-lg-12 form-group pull-left">
                      <label for="email">Spesifikasi Produk *:</label>
                      <textarea type="text" class="form-control" name="spesifikasi_produk" placeholder="Masukkan spesifikasi produk" required></textarea>
                      <?php echo form_error('spesifikasi_produk'); ?>
                </div>
               
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-default">Submit</button>
                    <button onclick="goBack()" class="btn btn-default">Kembali</button>
                </div>
            </form> 
        </div>
    </div>
</section>
<script>
    function goBack() {
        window.history.back();
    }
    
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
 
</script>