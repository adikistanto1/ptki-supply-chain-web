<section class="content-header">
    <h1 style="margin-bottom: 15px">
        Welcome...
    </h1>
    <div class="panel panel-default">
        <div class="panel-body">
           
          <div class="row">
            <div class="col-sm-6">
              <div class="card">
                <div class="card-body">
                  <h3 class="card-title">Features</h3>
                  <p class="card-text">
                      1. Kelola Order <br>
                         - membuat order baru terhadap vendor tertentu, yang memungkinkan menambah banyak produk dalam tiap satu order. Jika hanya satu juga bisa. 
                         Pertama kali dibuat status order draf, setelah ditambahkan produk baru bisa berubah status dikirim ke vendor. Ketika dikirim vendor akan menerima notif di aplikasi android.<br>
                      2. Kelola Produk <br>
                         - menambah produk - produk baru<br>
                        3. Kelola Vendor <br>
                        - menambah evendor-vendor baru<br>
                        4. Lihat Stok <br>
                        - untuk pertimbangan order produk<br>
                        5. Pembayaran <br>
                        - Memantau status pembayaran order yang sudah dicek. Tenggat bayar + 1 bulan sejak pengecekan oleh QC<br><br>
                        
                        masing - masing menu punya fitur cetak listing, mestinya bisa difilter lagi list yang ingin dicetak
                  </p>
                  <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
</section>