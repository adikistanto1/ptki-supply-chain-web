<section class="content-header">
    <h1 style="margin-bottom: 15px">
        Detail Order
    </h1>
    
<!--    <a style="margin-bottom: 15px" class="btn btn-info" href="<?php echo base_url(); ?>administrasi/orderform">Tambah</a>-->

    <?php
        
        $message = $this->session->flashdata('message');
        if (isset($error)) {
            echo"<div class='alert alert-warning' role='alert'>" . $error . "<button type='button' class='close' data-dismiss='alert'' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button></div>";
        } else if(isset ($message)) {
            echo"<div class='alert alert-success' role='alert'>" . $message . "<button type='button' class='close' data-dismiss='alert'' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button></div>";
            $this->session->unset_userdata('message');
        }
        
        $total_harga = 0;
        if(!empty($produkorder)){
            $total = 0;
            foreach($produkorder as $item): 
                    $harga_item     =  $item['harga_produk_order']; 
                    $jml_item       =  $item['jumlah_produk'];
                    $total          = $harga_item * $jml_item; 
                    $total_harga    = $total_harga + $total;
            endforeach;
        }
        
    ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-4">
                    <h3>Order</h3>
                    <table class="table table-hover" >

                        <tr>
                            <td><b>Nomor Order</b></td>
                            <td>:&nbsp; <?php echo 'PO-'.$order['id_order']; ?></td>
                        </tr
                        <tr>
                            <td><b>Vendor</b></td>
                            <td>:&nbsp; <?php echo $order['nama_vendor']; ?></td>
                        </tr>
                        <tr>
                            <td><b>Tanggal Buat</b></td>
                            <td>:&nbsp;<?php echo date("d/m/y h:i", strtotime($order['timestamp'])); ?></td>
                        </tr>
                         <tr>
                            <td><b>Status Order</b></td>
                            <td>:&nbsp;
                                <?php 
                                    if($order['status_order']==0){
                                        echo "DRAFT";   
                                    }else if($order['status_order']==1){
                                        echo "DIKIRIM KE VENDOR";
                                    }else if($order['status_order']==2){
                                        echo "Proses Produksi";
                                    }else if($order['status_order']==3){
                                        echo "Selesai Produksi";
                                    }else if($order['status_order']==4){
                                        echo "Dikirim";
                                    }else if($order['status_order']==5){
                                        echo "Selesai";
                                    };
                                ?>
                                                
                            </td>
                            <tr>
                            <td><b>Keterangan</b></td>
                            <td>:&nbsp;<?php echo $order['keterangan'];?></td>
                        </tr>
                        </tr>
                           
                    </table>
                </div>
                <div class="col-sm-8">
                    <h3>Tambah Produk</h3>
                     <?php if($order['status_order']==0):?>
                    <form class="form-inline" method="post" action="<?php echo base_url(); ?>purchase/produkordersimpan">
                        <div class="form-group">
                             <label for="email">Produk *:</label>
                             <select class="js-example-basic-single form-control" name="id_produk" autofocus="">
                                   <option value="">Pilih</option>
                                <?php foreach($produk as $item):?>
                                 
                                  <option value="<?php echo $item['id_produk'].'.'.$item['harga_produk']?>"><?php echo $item['nama_produk']."  (".$item['satuan_produk'].")"?></option>
                                   
                                <?php endforeach; ?>
                              </select>
                              
                              <label for="email">Jumlah *:</label>
                              <input name="jumlah_produk" type="number" min="1" placeholder="Masukkan jumlah"class="form-control" required=""/>
                              
                              <label for="email">Tgl Datang *:</label>
                              <input name="tanggal_target_kirim" id="datepicker" placeholder="Masukkan tanggal"class="form-control" required=""/>
                              
                             <input name="id_order" type="hidden" value="<?php echo $order['id_order']?>"/>
                            <button type="submit" class="btn btn-info">Tambah</button>
                        </div>                        
                    </form>
                    <?php endif;?><br>
                    <table class="table table-hover" >
                        <tr>
                            <th class="text-center">No</th>
                            <th class="text-center">Nama Produk</th>
                            <th class="text-center">Harga Produk</th>
                            <th class="text-center">Jumlah</th>
                            <th class="text-center">Tanggal Datang</th>
                            <?php if($order['status_order']==0):?>
                            <th class="text-center">Action</th>
                            <?php endif;?>
                        </tr>
                        <?php if(empty($produkorder)): ?>
                        <tr>
                            <td colspan="7" class="text-center">Belum ada produk</td>
                        </tr>
                        <?php else: 
                            $key = 0;
                            $is_stok_minus = FALSE;
                            $is_minus_checked = FALSE;
                            ?>
                            <?php foreach($produkorder as $item): 
                                $key = $key+1?>
                            <tr>
                                <td class="text-center"><?php echo $key; ?></td>
                                <td class="text-center">
                                    <?php echo $item['nama_produk']; ?>
                                </td>
                                <td class="text-center">
                                    <?php echo number_format($item['harga_produk'] , 2 , ',','.' ); ?>                                  
                                </td>
                                 <td class="text-center">
                                    <?php echo $item['jumlah_produk']." ".$item['satuan']; ?>
                                </td>
                                <td class="text-center"><?php echo date("d/m/y", strtotime($item['tanggal_target_kirim'])); ?></td>
                                <?php if($order['status_order']==0):?>
                                <td width="60px">
                                    <form onsubmit="return confirmHapus(this);"id="myForm" method="POST" action="<?php echo base_url(); ?>purchase/produkorderhapus">
                                        <input name="id_produk_order" type="hidden" value="<?php echo $item['id_produk_order']?>"/>
                                        <input name="id_order" type="hidden" value="<?php echo $order['id_order']?>"/>
                                        <input type="submit" class="btn btn-danger" value="Hapus"/>
                                    </form>      
                                </td>
                                <?php endif;?>
                                
                            </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </table>
                    <?php if(($order['status_order']==0)&&($produkorder!=null)):?>
                        <form  class="form-group" method="POST" action="<?php echo base_url(); ?>purchase/orderupdatestatus">
                            <input name="id_order" type="hidden" value="<?php echo $item['id_order']?>"/>
                            <input name="status_order" type="hidden" value="1"/>
                            <input type="submit" class="btn btn-success" value="Kirim ke Vendor"/>
                        </form>
                    <?php endif;?>
                </div>
                <div class="col-sm-12" style="margin-top: 20px">
                    <hr>                                          
                    <a href="<?php echo base_url();?>purchase/order" class="btn btn-default">Kembali</a>                   
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    function confirmHapus(form){
         return confirm('Anda yakin akan mengahapus ini ?');
    }
    
    function confirmHapusProsesProduksi(form){
         return confirm('Anda yakin akan mengahapus ini ?\nMenghapus data ini berarti menghapus juga data produksi produk ini ');
    }
    
     
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
        $('#datepicker').datepicker({
            todayHighlight: true,
            format: 'yyyy-mm-dd',
            autoclose:true,
            startDate: new Date()
        });
    });
    
    
    
</script>
