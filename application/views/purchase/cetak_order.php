<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/bootstrap/css/bootstrap.min.css">
<style>
th, td {
    padding: 5px;
}
</style>
<section class="content-header">
    <h1 style="margin-bottom: 15px">
        Daftar Order
    </h1>
    
     <div class="panel panel-default">
        <div class="panel-body">
            <table id="table" class="table table-bordered">
                <thead>
                <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">Nomor Order</th>
                    <th class="text-center">Vendor</th>
                    <th class="text-center">Keterangan</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Jumlah Produk</th>
                    <th class="text-center">Tanggal Buat</th>
                </tr>
                </thead>
                <tbody>
                <?php if(empty($order)): ?>
                <!--<tr>-->
                <!--    <td colspan="7" class="text-center">Data tidak ditemukan</td>-->
                <!--</tr>-->
                <?php else: 
                    $key = 0;?>
                    <?php foreach($order as $item): 
                        $key = $key+1?>
                    <tr>
                        <td class="text-center"><?php echo $key; ?></td>
                        <td class="text-center">
                            <?php echo 'PO-'.$item['id_order']; ?>
                        </td>
                        <td class="text-center">
                            <?php echo $item['nama_vendor']; ?>
                        </td>
                        <td class="text-center">
                            <?php echo $item['keterangan']; ?>
                        </td>
                        <td class="text-center">
                            <?php 
                            if($item['status_order']==0){
                                echo "DRAFT";   
                            }else if($item['status_order']==1){
                                echo "DIKIRIM KE VENDOR";
                            }else if($item['status_order']==2){
                                echo "Proses Produksi";
                            }else if($item['status_order']==3){
                                echo "Selesai Produksi";
                            }else if($item['status_order']==4){
                                echo "Dikirim";
                            }else if($item['status_order']==5){
                                echo "Selesai";
                            }; ?>
                        </td>
                        <td class="text-center">
                            <?php 
                                if($produkorder!=null){
                                    $print = FALSE;
                                    foreach($produkorder as $item_alokasi): 
                                    if($item['id_order']==$item_alokasi['id_order']){                                 
                                        $is_alocated = TRUE;

                                        $content =  $item_alokasi['count_produk']." produk";
                                        $print = TRUE;


                                    }else{                                    
                                        $is_alocated = FALSE;

                                        if(!$print){
                                            $content =  'belum ada';
                                            $print = TRUE;
                                        }
                                    }
                                    endforeach;

                                    echo $content;
                                }else{
                                    echo 'belum ada';
                                }
                                
                            ?>  
                        </td>
                        <td class="text-center">
                            <?php echo date("d/m/y h:i", strtotime($item['timestamp'])); ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<script>
    $(function () {
	$("#table").DataTable();
    });
    
    function confirmHapus(form){
         return confirm('Anda yakin akan menghapus data ini ?');
    }
</script>