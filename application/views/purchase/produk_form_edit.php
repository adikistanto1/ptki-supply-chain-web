<section class="content-header">
    <h1 style="margin-bottom: 15px">
        Ubah Produk
    </h1>
    
    <?php
        $message = $this->session->flashdata('message');
        $error1 = $this->session->flashdata('error');
        if (isset($error)) {
            echo"<div class='alert alert-warning' role='alert'>" . $error . "</div>";
        } else if(isset ($message)) {
            echo"<div class='alert alert-success' role='alert'>" . $message . "</div>";
            $this->session->unset_userdata('message');
        }else if(isset ($error1)) {
            echo"<div class='alert alert-warning' role='alert'>" . $error1 . "</div>";
            $this->session->unset_userdata('error');
        }
    ?>
    
    <div class="panel panel-default">
        <div class="panel-body">
            <form method="post" action="<?php echo base_url(); ?>purchase/produksimpanedit">
                <div class="col-lg-4 form-group">
                      <label for="email">Nama Produk *:</label>
                      <input type="text" class="form-control" name="nama_produk" required  value="<?php echo $produk['nama_produk']?>">
                      <?php echo form_error('nama_produk'); ?>
                </div>
                 <div class="col-lg-4 form-group">
                    <label for="email">Kategori *:</label>
                    <select class="js-example-basic-single form-control" name="id_kategori_produk" required="">
                            <option value="">Pilih Costumer</option>
                            <?php foreach($kategori as $item):?>

                               <option value="<?php echo $item['id_kategori_produk'];?>" <?php if($item['id_kategori_produk']==$produk['id_kategori_produk']){echo 'selected';}?>><?php echo $item['nama_kategori_produk']?></option>

                            <?php endforeach; ?>
                    </select>
                </div>
                 <div class="col-lg-4 form-group">
                      <label for="pwd">Harga Produk *:</label>
                      <input type="number" class="form-control" name="harga_produk" placeholder="Masukkan harga produk" value="<?php echo $produk['harga_produk']?>" required>
                      <?php echo form_error('harga_produk'); ?>
                </div>
                <div class="col-lg-4 form-group">
                      <label for="pwd">Satuan *:</label>
                      <select class="js-example-basic-single form-control" name="satuan_produk" required="">
                         <option value="">Pilih Satuan</option>
                         <option value="Pack" <?php if($produk['satuan_produk']=="Pack"){echo "selected";}?>>Pack</option>
                         <option value="Dus"  <?php if($produk['satuan_produk']=="Dus"){echo "selected";}?>>Dus</option>
                         <option value="Pack" <?php if($produk['satuan_produk']=="Pack"){echo "selected";}?>>Pack</option>
                         <option value="Item"  <?php if($produk['satuan_produk']=="Item"){echo "selected";}?>>Item</option>
                    </select>
                </div>
                <div class="col-lg-12 form-group">
                      <label for="email">Spesifikasi Produk *:</label>
                      <textarea type="text" class="form-control" name="spesifikasi_produk" placeholder="Masukkan spesifikasi produk"  required><?php echo $produk['spesifikasi_produk']?></textarea>
                      <?php echo form_error('spesifikasi_produk'); ?>
                </div>
                
                <input type="hidden" name="id_produk" value="<?php echo $produk['id_produk']?>"/>
               
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-default">Submit</button>
                    <button onclick="goBack()" type="button" class="btn btn-default">Kembali</button>
                </div>
                
            </form> 
        </div>
    </div>
</section>
<script>
    function goBack() {
        window.history.back();
    }
    
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
</script>