<section class="content-header">
    <h1 style="margin-bottom: 15px">
        Daftar Stok
    </h1>

    <?php
        $message = $this->session->flashdata('message');
        if (isset($error)) {
            echo"<div class='alert alert-warning' role='alert'>" . $error . "<button type='button' class='close' data-dismiss='alert'' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button></div>";
        } else if(isset ($message)) {
            echo"<div class='alert alert-success' role='alert'>" . $message . "<button type='button' class='close' data-dismiss='alert'' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button></div>";
        }
    ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <table id="table" class="table table-hover">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Produk</th>
                    <th class="text-center">Kategori</th>
                    <th class="text-center">Stok</th>
                </tr>
                </thead>
                <tbody>
                <?php if(empty($stok)): ?>
                <!--<tr>-->
                <!--    <td colspan="7" class="text-center">Data tidak ditemukan</td>-->
                <!--</tr>-->
                <?php else: 
                    $key = 0;?>
                    <?php foreach($stok as $item): 
                        $key = $key+1?>
                    <tr>
                        <td><?php echo $key; ?></td>
                        <td>
                            <?php echo $item['nama_produk']; ?>
                        </td>
                        <td class="text-center">
                            <?php echo $item['nama_kategori_produk']; ?>
                        </td>
                         <td class="text-center">
                            <?php echo $item['stok']; ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
            <a style="margin-bottom: 15px" target="_blank"class="btn btn-info uppercase" href="<?php echo base_url(); ?>purchase/stokcetak">Cetak Stok</a>
        </div>
    </div>
</section>
<script>
    $(function () {
	$("#table").DataTable();
    });
    
    function confirmHapus(form){
         return confirm('Anda yakin akan menghapus data ini ?');
    }
</script>