<section class="content-header">
    <h1 style="margin-bottom: 15px">
        Tambah Vendor
    </h1>
    
    <?php
        $message = $this->session->flashdata('message');
        $error1 = $this->session->flashdata('error');
        if (isset($error)) {
            echo"<div class='alert alert-warning' role='alert'>" . $error . "</div>";
        } else if(isset ($message)) {
            echo"<div class='alert alert-success' role='alert'>" . $message . "</div>";
            $this->session->unset_userdata('message');
        }else if(isset ($error1)) {
            echo"<div class='alert alert-warning' role='alert'>" . $error1 . "</div>";
            $this->session->unset_userdata('error');
        }
    ?>
    
    <div class="panel panel-default">
        <div class="panel-body">
            <form method="post" action="<?php echo base_url(); ?>purchase/vendorsimpan">
                <div class="col-lg-4 form-group">
                      <label for="email">Nama Vendor *:</label>
                      <input type="text" class="form-control" name="nama_vendor" placeholder="Masukkan nama vendor" required>
                      <?php echo form_error('nama_produk'); ?>
                </div>
                
                
                
                <div class="col-lg-4 form-group">
                      <label for="pwd">Telpon *:</label>
                      <input type="text" class="form-control" name="telpon_vendor" placeholder="Masukkan nomor telpon" required>
                      <?php echo form_error('harga_produk'); ?>
                </div>
               
                <div class="col-lg-12 form-group pull-left">
                      <label for="email">Alamat *:</label>
                      <textarea type="text" class="form-control" name="alamat_vendor" placeholder="Masukkan alamat vendor" required></textarea>
                      <?php echo form_error('alamat_vendor'); ?>
                </div>
                      
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-default">Submit</button>
                    <button onclick="goBack()" class="btn btn-default">Kembali</button>
                </div>
            </form> 
        </div>
    </div>
</section>
<script>
    function goBack() {
        window.history.back();
    }
    
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
    
    
    
</script>