<section class="content-header">
    <h1 style="margin-bottom: 15px">
        Daftar Order
    </h1>
    
    <a style="margin-bottom: 15px" class="btn btn-info" href="<?php echo base_url(); ?>purchase/orderform">Tambah</a>

    <?php
        $message = $this->session->flashdata('message');
        if (isset($error)) {
            echo"<div class='alert alert-warning' role='alert'>" . $error . "<button type='button' class='close' data-dismiss='alert'' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button></div>";
        } else if(isset ($message)) {
            echo"<div class='alert alert-success' role='alert'>" . $message . "<button type='button' class='close' data-dismiss='alert'' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button></div>";
        }
    ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <table id="table" class="table table-hover">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nomor Order</th>
                    <th>Vendor</th>
                    <th>Keterangan</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Jumlah Produk</th>
                    <th class="text-center">Tanggal Buat</th>
                    <th class="text-center">Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php if(empty($order)): ?>
                <!--<tr>-->
                <!--    <td colspan="7" class="text-center">Data tidak ditemukan</td>-->
                <!--</tr>-->
                <?php else: 
                    $key = 0;?>
                    <?php foreach($order as $item): 
                        $key = $key+1?>
                    <tr>
                        <td><?php echo $key; ?></td>
                        <td>
                            <?php echo 'PO-'.$item['id_order']; ?>
                        </td>
                        <td>
                            <?php echo $item['nama_vendor']; ?>
                        </td>
                        <td>
                            <?php echo $item['keterangan']; ?>
                        </td>
                        <td class="text-center">
                            <?php 
                            if($item['status_order']==0){
                                echo "DRAFT";   
                            }else if($item['status_order']==1){
                                echo "DIKIRIM KE VENDOR";
                            }else if($item['status_order']==2){
                                echo "Proses Produksi";
                            }else if($item['status_order']==3){
                                echo "Selesai Produksi";
                            }else if($item['status_order']==4){
                                echo "Dikirim";
                            }else if($item['status_order']==5){
                                echo "Selesai";
                            }; ?>
                        </td>
                        <td class="text-center">
                            <?php 
                                if($produkorder!=null){
                                    $print = FALSE;
                                    foreach($produkorder as $item_alokasi): 
                                    if($item['id_order']==$item_alokasi['id_order']){                                 
                                        $is_alocated = TRUE;

                                        $content =  $item_alokasi['count_produk']." produk";
                                        $print = TRUE;


                                    }else{                                    
                                        $is_alocated = FALSE;

                                        if(!$print){
                                            $content =  'belum ada';
                                            $print = TRUE;
                                        }
                                    }
                                    endforeach;

                                    echo $content;
                                }else{
                                    echo 'belum ada';
                                }
                                
                            ?>  
                        </td>
                        <td class="text-center">
                            <?php echo date("d/m/y h:i", strtotime($item['timestamp'])); ?>
                        </td>
                       
                        <td width="60px">
                            <form  method="POST" action="<?php echo base_url(); ?>purchase/orderdetail">
                                <input name="id_order" type="hidden" value="<?php echo $item['id_order']?>"/>
                                <input type="submit" class="btn btn-success" value="Detail"/>
                            </form>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
            <a style="margin-bottom: 15px" target="_blank"class="btn btn-info uppercase" href="<?php echo base_url(); ?>purchase/ordercetak">Cetak Daftar Order</a>
        </div>
    </div>
</section>
<script>
    $(function () {
	$("#table").DataTable();
    });
    
    function confirmHapus(form){
         return confirm('Anda yakin akan menghapus data ini ?');
    }
</script>