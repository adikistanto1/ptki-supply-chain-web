<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/bootstrap/css/bootstrap.min.css">
<style>
th, td {
    padding: 10px;
}
</style>
<div id="isi">
    <table width="100%" style="background-color: white;font-size: 12px">
        <tr>
            <th width="30%">
                <b><?php echo $order['nama_vendor'];?></b><br>
                <?php echo $order['alamat_vendor'];?><br>
                <?php echo $order['telpon_vendor'];?>
            </th>
            <th width="50%" style="background-color: white;font-size: 20px">
                <center><b>DELIVERY SHEET</b></center>
            </th>
        </tr>
    </table>
    <br>
    <table cellpadding="15" class="table table-bordered" style="background-color: white;font-size: 12px;" >
        <?php 
            foreach($produkorder as $item){
                $id_produk_order = $item['id_produk_order'];
            }
        ?>
     
        <tr>
            <td colspan="2">Nomor Order</td>
            <td colspan="2"><?php echo 'PO-'.$order['id_order'];?></td>
            <td class="text-center" rowspan="4">
                <barcode code="<?php echo $id_produk_order;?>" type="QR" class="barcode" size="1.2" error="M" />
            </td>
        </tr>
        <tr>
            <td colspan="2">Tanggal pemesanan</td>
            <td colspan="2"><?php echo date("d", strtotime($order['timestamp']))." ".convert_month(date("m", strtotime($order['timestamp'])))." ".date("Y", strtotime($order['timestamp']));?></td>
        </tr>
        <tr>
            <td colspan="2">Buyer</td>
            <td colspan="2">PT. Kubota Indonesia</td>
        </tr>      
        <tr>
            <td colspan="2">Supplier</td>
            <td colspan="2"><?php echo $order['nama_vendor'];?></td>
        </tr>
       
                 
        <tr>
            <th colspan="5"><b>Produk</b></th>                       
        </tr>
        
        <tr>
            <th width="20" class="text-center">No</th>
            <th class="text-center">Nama Produk</th>
            <th colspan="2" class="text-center">Spesifikasi</th>
            <th class="text-center">Jumlah</th>
        </tr>
        <?php 
        $key = 0;
        $total_harga = 0;
        foreach($produkorder as $item): 
            $key = $key+1?>
        <tr>
            <td width="8%" class="text-center"><?php echo $key; ?></td>
            <td width="25%">
                <?php echo $item['nama_produk']; ?>
            </td>
            <td colspan="2"  width="30%" class="text-center">
                <?php echo $item['spesifikasi_produk']; ?>
            </td>
            <td width="30%" class="text-center">
                <?php echo $item['jumlah_produk']; ?>
            </td>
            
        </tr>
        <?php endforeach;?>
    </table>
    
</div>

<?php
    function convert_month($kode){
        if($kode==1){
            return 'Januari';
        }else if($kode==2){
            return 'Februari';
        }else if($kode==3){
            return 'Maret';
        }else if($kode==4){
            return 'April';
        }else if($kode==5){
            return 'Mei';
        }else if($kode==6){
            return 'Juni';
        }else if($kode==7){
            return 'Juli';
        }else if($kode==8){
            return 'Agustus';
        }else if($kode==9){
            return 'September';
        }else if($kode==10){
            return 'Oktober';
        }else if($kode==11){
            return 'November';
        }else if($kode==12){
            return 'Desember';
        }
    } 
?>