<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/bootstrap/css/bootstrap.min.css">
<style>
th, td {
    padding: 5px;
}
</style>
<section class="content-header">
    <h1 style="margin-bottom: 15px">
        Daftar Pemasukan
    </h1>

 
    <div class="panel panel-default">
        <div class="panel-body">
            <table id="table" class="table table-bordered">
                <thead>
                <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">Nama Produk</th>
                    <th class="text-center">Jumlah Masuk</th>
                    <th class="text-center">Waktu Masuk</th>
                    <th class="text-center">Supplier</th>
                </tr>
                </thead>
                <tbody>
                <?php if(empty($pemasukan)): ?>
                <!--<tr>-->
                <!--    <td colspan="7" class="text-center">Data tidak ditemukan</td>-->
                <!--</tr>-->
                <?php else: 
                    $key = 0;?>
                    <?php foreach($pemasukan as $item): 
                        $key = $key+1?>
                    <tr>
                        <td class="text-center"><?php echo $key; ?></td>
                        <td class="text-center">
                            <?php echo $item['nama_produk']; ?>
                        </td>
                        <td class="text-center">
                            <?php echo $item['jumlah_masuk']; ?>
                        </td>
                        <td class="text-center">
                            <?php echo date("d/m/y h:i", strtotime($item['waktu_masuk'])); ?>
                        </td>
                         <td class="text-center">
                            <?php echo $item['nama_vendor']; ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
            
        </div>
    </div>
</section>
<script>
    $(function () {
	$("#table").DataTable();
    });
    
    function confirmHapus(form){
         return confirm('Anda yakin akan menghapus data ini ?');
    }
</script>