<section class="content-header">
    <h1 style="margin-bottom: 15px">
        Tambah Pengeluaran
    </h1>
    
    <?php
        $message = $this->session->flashdata('message');
        $error1 = $this->session->flashdata('error');
        if (isset($error)) {
            echo"<div class='alert alert-warning' role='alert'>" . $error . "</div>";
        } else if(isset ($message)) {
            echo"<div class='alert alert-success' role='alert'>" . $message . "</div>";
            $this->session->unset_userdata('message');
        }else if(isset ($error1)) {
            echo"<div class='alert alert-warning' role='alert'>" . $error1 . "</div>";
            $this->session->unset_userdata('error');
        }
    ?>
    
    <div class="panel panel-default">
        <div class="panel-body">
            <form method="post" action="<?php echo base_url(); ?>gudang/pengeluaransimpan">
                
                <div class="col-lg-4 form-group">
                    <label for="email">Produk *:</label>
                    <select class="js-example-basic-single form-control" name="id_produk" required="">
                        <option value="">Pilih Produk</option>
                        <?php foreach($produk as $item):?>

                            <option value="<?php echo $item['id_produk'];?>"><?php echo $item['nama_produk']?></option>

                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="row"></div>
                <div class="col-lg-4 form-group">
                      <label for="pwd">Jumlah Produk *:</label>
                      <input type="number" class="form-control" name="jumlah_produk" placeholder="Masukkan jumlah produk" required>
                      <?php echo form_error('jumlah_produk'); ?>
                </div>
                    
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-default">Submit</button>
                    <button type="button" onclick="goBack()" class="btn btn-default">Kembali</button>
                </div>
            </form> 
        </div>
    </div>
</section>
<script>
    function goBack() {
        window.history.back();
    }
    
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
 
</script>