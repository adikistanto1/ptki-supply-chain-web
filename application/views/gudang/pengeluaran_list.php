<section class="content-header">
    <h1 style="margin-bottom: 15px">
        Daftar Pengeluaran
    </h1>
    
    <a style="margin-bottom: 15px" class="btn btn-info" href="<?php echo base_url(); ?>gudang/pengeluaranform">Tambah</a>

    <?php
        $message = $this->session->flashdata('message');
        if (isset($error)) {
            echo"<div class='alert alert-warning' role='alert'>" . $error . "<button type='button' class='close' data-dismiss='alert'' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button></div>";
        } else if(isset ($message)) {
            echo"<div class='alert alert-success' role='alert'>" . $message . "<button type='button' class='close' data-dismiss='alert'' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button></div>";
        }
    ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <table id="table" class="table table-hover">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Produk</th>
                    <th class="text-center">Jumlah Pengeluaran</th>
                    <th class="text-center">Waktu</th>
                    <th class="text-center">Ubah</th>
                    <th class="text-center">Hapus</th>
                </tr>
                </thead>
                <tbody>
                <?php if(empty($pengeluaran)): ?>
                <!--<tr>-->
                <!--    <td colspan="7" class="text-center">Data tidak ditemukan</td>-->
                <!--</tr>-->
                <?php else: 
                    $key = 0;?>
                    <?php foreach($pengeluaran as $item): 
                        $key = $key+1?>
                    <tr>
                        <td><?php echo $key; ?></td>
                        <td>
                            <?php echo $item['nama_produk']; ?>
                        </td>
                        <td class="text-center">
                            <?php echo $item['jumlah_keluar']; ?>
                        </td>
                        <td class="text-center">
                            <?php echo date("d/m/y h:i", strtotime($item['waktu_keluar'])); ?>
                        </td>
                       
                        <td width="60px">
                            <form  method="POST" action="<?php echo base_url(); ?>gudang/pengeluaranformedit">
                                <input name="id_pengeluaran" type="hidden" value="<?php echo $item['id_pengeluaran']?>"/>
                                <input type="submit" class="btn btn-warning" value="Ubah"/>
                            </form>
                        </td>
                        <td width="60px">
                            <form onsubmit="return confirmHapus(this);"id="myForm" method="POST" action="<?php echo base_url(); ?>gudang/pengeluaranhapus">
                                <input name="id_pengeluaran" type="hidden" value="<?php echo $item['id_pengeluaran']?>"/>
                                <input type="submit" class="btn btn-danger" value="Hapus"/>
                            </form>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
            <a style="margin-bottom: 15px" target="_blank"class="btn btn-info uppercase" href="<?php echo base_url(); ?>gudang/pengeluarancetak">Cetak Daftar Pengeluaran</a>
        </div>
    </div>
</section>
<script>
    $(function () {
	$("#table").DataTable();
    });
    
    function confirmHapus(form){
         return confirm('Anda yakin akan menghapus data ini ?');
    }
</script>