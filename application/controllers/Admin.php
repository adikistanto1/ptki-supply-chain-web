<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    
    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('logged_in')) {
            redirect('auth');
        } else{
           if ($this->session->userdata('role')!="Admin") {
                redirect('auth/loginform');
           } 
        }

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('M_User');
        $this->load->model('M_Vendor');
    }


    public function index()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
         
        $this->load->viewAdmin('admin/dashboard',$data);
    }
    
    public function akun($type)
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        
        switch ($type){
            case 'purchase' :
                $data['role_code']  = '2';
                $data['role_name']  = $type;
                $data['list']       = $this->M_User->get_list_akun(2); 
                break;
            case 'penerima' :
                $data['role_code']  = '3';
                $data['role_name']  = $type;
                $data['list']       = $this->M_User->get_list_akun(3);
                break;
            case 'qc' :
                $data['role_code']  = '4';
                $data['role_name']  = $type;
                $data['list']       = $this->M_User->get_list_akun(4);
                break;
            case 'gudang' :
                $data['role_code']  = '5';
                $data['role_name']  = $type;
                $data['list']       = $this->M_User->get_list_akun(5);
                break;
            case 'vendor' :
                $data['role_code']  = '6';
                $data['role_name']  = $type;
                $data['list']       = $this->M_User->get_list_akun_vendor();
                break;
        }    
        
        $this->load->viewAdmin('admin/akun_list',$data);
    }
    
    public function akunform($type)
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        
        switch ($type){
            case 'purchase' :
                $data['role_code']  = '2';
                $data['role_name']  = $type;
                $data['list']       = $this->M_User->get_list_akun(2); 
                break;
            case 'penerima' :
                $data['role_code']  = '3';
                $data['role_name']  = $type;
                $data['list']       = $this->M_User->get_list_akun(3);
                break;
            case 'qc' :
                $data['role_code']  = '4';
                $data['role_name']  = $type;
                $data['list']       = $this->M_User->get_list_akun(4);
                break;
            case 'gudang' :
                $data['role_code']  = '5';
                $data['role_name']  = $type;
                $data['list']       = $this->M_User->get_list_akun(5);
                break;
            case 'vendor' :
                $data['role_code']  = '6';
                $data['role_name']  = $type;
                $data['list']       = $this->M_User->get_list_akun_vendor();
                $data['vendor']     = $this->M_Vendor->get_list_vendor();
                break;
        }    
        
        $this->load->viewAdmin('admin/akun_form',$data);
    }

    public function akunsimpan($type,$id=false){
        
        $data['error'] = 'Error, harap isi formulir dengan benar !';

        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        //set_rules validasi
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|sha1');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->viewAdmin('admin/akun_form/'.$type,$data);	
        }else{
            if ($id === false) {
                $this->session->set_flashdata('message', 'Berhasil menambah data... ');
            } else {
                $this->session->set_flashdata('message', 'Berhasil mengubah data... ');
            }

            if($type=='vendor'){
                $this->M_User->simpan_akun1($id);
            }else{
                $this->M_User->simpan_akun($id);
            }
 
            redirect('admin/akun/'.$type);
        }

    }
    
    public function akunhapus($type) {
        $id = $this->input->post('id');
        $this->M_User->hapus_akun($id);
        $this->session->set_flashdata('message', 'Berhasil menghapus data... ');
        redirect('admin/akun/'.$type);
    } 
}
