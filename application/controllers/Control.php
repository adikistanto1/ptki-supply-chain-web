<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Control extends CI_Controller {
    
    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('logged_in')) {
            redirect('auth');
        } else{
           if ($this->session->userdata('role')!="Control") {
                redirect('auth/loginform');
           } 
        }

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('M_User');
        $this->load->model('M_Produk');
        $this->load->model('M_Kategori_Produk');
        $this->load->model('M_Vendor');
        $this->load->model('M_Order');
        $this->load->model('M_Produk_Order');
        $this->load->model('M_Penerimaan');
        $this->load->model('M_Pengecekan');

    }


    public function index()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
         
        $this->load->viewQC('qc/dashboard',$data);
    }
    
    public function produkorder()
    {
        $data['nama']           = $this->session->userdata('name');
        $data['role']           = $this->session->userdata('role');    
        $data['produkorder']    = $this->M_Pengecekan->get_list_pengecekan();
        $this->load->viewQC('qc/produk_order_list',$data);
    }
    
    public function produkcek()
    {
        $data['nama']           = $this->session->userdata('name');
        $data['role']           = $this->session->userdata('role');    
        $data['produkcek']    = $this->M_Pengecekan->get_list_pengecekan_cek();
        $this->load->viewQC('qc/produk_order_list_cek',$data);
    }
    
    
    public function orderupdatestatus(){
        
        $data['error'] = 'Error, harap isi formulir dengan benar !';

        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
       
        $status     = $this->input->post('status_order');
        $id         = $this->input->post('id_order');
        
        $this->session->set_flashdata('message', 'Berhasil mengubah data... ');
        $this->M_Order->ubah_status_order($status,$id);
        redirect('purchase/order');
        

    }
    
    public function cetakds()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
      
        $id_order           = $this->input->post('id_order');
        $id_produk_order    = $this->input->post('id_produk_order');
        
        $data['order']          = $this->M_Order->get_order_id($id_order);
        $data['produkorder']    = $this->M_Produk_Order->get_produk_order_id($id_produk_order);
        
        $html = $this->load->view('vendor/ds_cetak', $data,true);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }
    
    public function cetaksuratjalan()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
      
        $id_order           = $this->input->post('id_order');
        
        $data['order']          = $this->M_Order->get_order_id($id_order);
        $data['produkorder']    = $this->M_Produk_Order->get_produk_order($id_order);
        
        $html = $this->load->view('vendor/sj_cetak', $data,true);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }
    
    public function produkterimacetak()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
      
        $data['produkorder']    = $this->M_Pengecekan->get_list_pengecekan();
        
        $html = $this->load->view('qc/produk_order_list_cetak', $data,true);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }
    
    public function produkcekcetak()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
      
        $data['produkcek']    = $this->M_Pengecekan->get_list_pengecekan_cek();
        
        $html = $this->load->view('qc/produk_order_list_cek_cetak', $data,true);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }
}
