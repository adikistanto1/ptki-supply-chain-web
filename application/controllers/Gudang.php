<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gudang extends CI_Controller {
    
    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('logged_in')) {
            redirect('auth');
        } else{
           if ($this->session->userdata('role')!="Gudang") {
                redirect('auth/loginform');
           } 
        }

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('M_User');
        $this->load->model('M_Vendor');
        $this->load->model('M_Produk');
        $this->load->model('M_Pengeluaran');
    }


    public function index()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
         
        $this->load->viewGudang('gudang/dashboard',$data);
    }
    
    public function pengeluaran()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        
        $data['pengeluaran']    = $this->M_Pengeluaran->get_pengeluaran();
        $this->load->viewGudang('gudang/pengeluaran_list',$data);
    }
    
    public function pengeluaranform()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        
        $data['produk']    = $this->M_Produk->get_produk();
        $this->load->viewGudang('gudang/pengeluaran_form',$data);
    }
    
    public function pengeluaranformedit()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        
        $id                = $this->input->post('id_pengeluaran');
        $data['produk']    = $this->M_Produk->get_produk();
        $data['pengeluaran']    = $this->M_Pengeluaran->get_pengeluaran_id($id);
        $this->load->viewGudang('gudang/pengeluaran_form_edit',$data);
    }
    
    public function pengeluaransimpan(){
        
        $data['error'] = 'Error, harap isi formulir dengan benar !';

        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        //set_rules validasi
        $this->form_validation->set_rules('id_produk', 'Produk', 'trim|required');
        $this->form_validation->set_rules('jumlah_produk', 'Jumlah Produk', 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->viewPurchase('gudang/pengeluaran_form',$data);	
        }else{         
            $this->M_Pengeluaran->simpan_pengeluaran(false);
            $this->session->set_flashdata('message', 'Berhasil menambah data... ');
            redirect('gudang/pengeluaran');
        }

    }
    
    public function pengeluaransimpanedit(){
        
        $data['error'] = 'Error, harap isi formulir dengan benar !';

        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        //set_rules validasi
        $this->form_validation->set_rules('id_produk', 'Produk', 'trim|required');
        $this->form_validation->set_rules('jumlah_produk', 'Jumlah Produk', 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->viewPurchase('gudang/pengeluaran_form',$data);	
        }else{         
            $this->M_Pengeluaran->simpan_pengeluaran(true);
            $this->session->set_flashdata('message', 'Berhasil menambah data... ');
            redirect('gudang/pengeluaran');
        }

    }
    
    
    public function pengeluaranhapus() {
        $id = $this->input->post('id_pengeluaran');
        $this->M_Pengeluaran->hapus_pengeluaran($id);
        $this->session->set_flashdata('message', 'Berhasil menghapus data... ');
        redirect('gudang/pengeluaran');
    } 
    
    public function pemasukan()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        
        $data['pemasukan']    = $this->M_Pengeluaran->get_pemasukan();
        $this->load->viewGudang('gudang/pemasukan_list',$data);
    }
    
    public function stok()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        
        $data['stok']    = $this->M_Pengeluaran->get_stok();
        $this->load->viewGudang('gudang/stok_list',$data);
    }
    
    public function stokcetak()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');

        
        $data['stok']    = $this->M_Pengeluaran->get_stok();
        
        $html = $this->load->view('gudang/cetak_stok', $data,true);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }
    
     public function pemasukancetak()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');

        
        $data['pemasukan']    = $this->M_Pengeluaran->get_pemasukan();
        
        $html = $this->load->view('gudang/cetak_pemasukan_list', $data,true);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }
    
     public function pengeluarancetak()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');

        
        $data['pengeluaran']    = $this->M_Pengeluaran->get_pengeluaran();
        
        $html = $this->load->view('gudang/cetak_pengeluaran_list', $data,true);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }
    
}
