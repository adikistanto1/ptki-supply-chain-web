<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('M_Auth');
	}
	public function index()
	{	
            if($this->session->userdata('logged_in')==false){
                $this->load->view('login');
            }else{
                $role = $this->session->userdata('role_code');
                $this->setRedirect($role);

            }
	}

        public function loginform(){
            $this->load->view('login');
        }
        
	public function login()
	{
            
		//set_rules validasi
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|sha1');
		
		if ($this->form_validation->run() === FALSE)//jika gagal/tidak memenuhi aturan
		{
                    $this->load->view('login');	
		}
		else //jika berhasil/memenuhi aturan
		{
			$row = $this->M_Auth->check_user($this->input->post('username'),$this->input->post('password'));//cek apakah user terdaftar
			//echo $this->input->post('username').' '.$this->input->post('password');return;
			if(!empty($row)){	//jika terdaftar
				$data_user = $this->M_Auth->get_user($this->input->post('username'),$this->input->post('password'));
                                var_dump($data_user);
				$sess_array = array(
				'id_user'   => $data_user['id_user'],
				'username'  => $data_user['username'],
                                'name'      => $data_user['name'],
                                'role_code' => $data_user['role'],
                                'role'      => $data_user['name_role'],
                                'id_vendor' => $data_user['id_vendor'],
				'logged_in' => TRUE
				);
				
				$this->session->set_userdata($sess_array);
				
                                $this->setRedirect($data_user['role']);
                                
			}else{
				$data['error'] = 'Gagal Login, username/password tidak terdaftar';
				$this->load->view('login',$data);
			}
		}
		
	}
        
        private function setRedirect($role){
            if($role == 1){
                return redirect('admin');
            }else if($role == 2){
                return redirect('purchase');
            }else if($role == 3){
                return redirect('penerima');
            }else if($role == 4){
                return redirect('control');
            }else if($role == 5){
                return redirect('gudang');
            }else if($role == 6){
                return redirect('vendors');
            }
        }

        public function logout(){
            if($this->session->userdata('logged_in')==false){
                    $this->load->view('login');
            }
            $this->session->sess_destroy();
            $this->load->view('login');
	}
	
}

/* End of file c_auth.php */
/* Location: ./application/controllers/admin/c_auth.php */