<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase extends CI_Controller {
    
    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('logged_in')) {
            redirect('auth');
        } else{
           if ($this->session->userdata('role')!="Purchase") {
                redirect('auth/loginform');
           } 
        }

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('M_User');
        $this->load->model('M_Produk');
        $this->load->model('M_Kategori_Produk');
        $this->load->model('M_Vendor');
        $this->load->model('M_Order');
        $this->load->model('M_Produk_Order');
        $this->load->model('M_Pengeluaran');
    }


    public function index()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        $this->load->viewPurchase('purchase/dashboard',$data);
    }
    
    //===============================PRODUK=======================================

    public function produk()
    {
        $data['nama']   = $this->session->userdata('name');
        $data['role']   = $this->session->userdata('role');
        $data['produk'] = $this->M_Produk->get_produk();
        $this->load->viewPurchase('purchase/produk_list',$data);
    }
    
    public function produkform()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        $data['kategori'] = $this->M_Kategori_Produk->get_kategori_produk();
        $this->load->viewPurchase('purchase/produk_form',$data);
    }
    
    public function produkformedit()
    {
        $data['nama']       = $this->session->userdata('name');
        $data['role']       = $this->session->userdata('role');
        $id                 = $this->input->post('id_produk');
        $data['produk']     = $this->M_Produk->get_produk_id($id);
        $data['kategori']   = $this->M_Kategori_Produk->get_kategori_produk();
        $this->load->viewPurchase('purchase/produk_form_edit',$data);
    }
    
    public function produksimpan(){
        
        $data['error'] = 'Error, harap isi formulir dengan benar !';

        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        //set_rules validasi
        $this->form_validation->set_rules('nama_produk', 'Nama Produk', 'trim|required');
        $this->form_validation->set_rules('spesifikasi_produk', 'Spesifikasi Produk', 'trim|required');
        $this->form_validation->set_rules('satuan_produk', 'Satuan Produk', 'trim|required');
        $this->form_validation->set_rules('id_kategori_produk', 'Kategori', 'trim|required');
        $this->form_validation->set_rules('harga_produk', 'Harga Produk', 'trim|required|numeric');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->viewPurchase('purchase/produk_form',$data);	
        }else{         
            $this->M_Produk->simpan_produk(false);
            $this->session->set_flashdata('message', 'Berhasil menambah data... ');
            redirect('purchase/produk');
        }

    }
    
    public function produksimpanedit(){
        
        $data['error'] = 'Error, harap isi formulir dengan benar !';

        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        //set_rules validasi
        $this->form_validation->set_rules('nama_produk', 'Nama Produk', 'trim|required');
        $this->form_validation->set_rules('spesifikasi_produk', 'Spesifikasi Produk', 'trim|required');
        $this->form_validation->set_rules('satuan_produk', 'Satuan Produk', 'trim|required');
        $this->form_validation->set_rules('id_kategori_produk', 'Kategori', 'trim|required');
        $this->form_validation->set_rules('harga_produk', 'Harga Produk', 'trim|required|numeric');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->viewPurchase('purchase/produk_form',$data);	
        }else{         
            $this->M_Produk->simpan_produk(TRUE);
            $this->session->set_flashdata('message', 'Berhasil mengubah data... ');
            redirect('purchase/produk');
        }

    }
    
    public function produkhapus() {
        $id = $this->input->post('id_produk');
        $this->M_Produk->hapus_produk($id);
        $this->session->set_flashdata('message', 'Berhasil menghapus data... ');
        redirect('purchase/produk');
    }
    
    //==================================== VENDOR =========================================
    
    public function vendor()
    {
        $data['nama']   = $this->session->userdata('name');
        $data['role']   = $this->session->userdata('role');
        $data['vendor'] = $this->M_Vendor->get_list_vendor();
        $this->load->viewPurchase('purchase/vendor_list',$data);
    }
    
    public function vendorform()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        $this->load->viewPurchase('purchase/vendor_form',$data);
    }
    
    public function vendorformedit()
    {
        $data['nama']       = $this->session->userdata('name');
        $data['role']       = $this->session->userdata('role');
        $id                 = $this->input->post('id_vendor');
        $data['vendor']     = $this->M_Vendor->get_vendor_id($id);
        $this->load->viewPurchase('purchase/vendor_form_edit',$data);
    }
    
    public function vendorsimpan(){
        
        $data['error'] = 'Error, harap isi formulir dengan benar !';

        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        //set_rules validasi
        $this->form_validation->set_rules('nama_vendor', 'Nama Vendor', 'trim|required');
        $this->form_validation->set_rules('telpon_vendor', 'Telpon', 'trim|required');
        $this->form_validation->set_rules('alamat_vendor', 'Alamat Vendor', 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->viewPurchase('purchase/vendor_form/',$data);	
        }else{
           
            $this->session->set_flashdata('message', 'Berhasil menambah data... ');
            $this->M_Vendor->simpan_vendor(FALSE);
            redirect('purchase/vendor');
        }

    }
    
    public function vendorsimpanedit(){
        
        $data['error'] = 'Error, harap isi formulir dengan benar !';

        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        //set_rules validasi
        $this->form_validation->set_rules('nama_vendor', 'Nama Vendor', 'trim|required');
        $this->form_validation->set_rules('telpon_vendor', 'Telpon', 'trim|required');
        $this->form_validation->set_rules('alamat_vendor', 'Alamat Vendor', 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->viewPurchase('purchase/vendor_form/',$data);		
        }else{
            $this->session->set_flashdata('message', 'Berhasil mengubah data... ');
            $this->M_Vendor->simpan_vendor(TRUE);
            redirect('purchase/vendor');
        }

    }
    
    public function vendorhapus() {
        $id = $this->input->post('id_vendor');
        $this->M_Vendor->hapus_vendor($id);
        $this->session->set_flashdata('message', 'Berhasil menghapus data... ');
        redirect('purchase/vendor');
    } 
    
    //=============================== ORDER =======================================

    public function order()
    {
        $data['nama']   = $this->session->userdata('name');
        $data['role']   = $this->session->userdata('role');    
        $data['order']  = $this->M_Order->get_order();
        $data['produkorder'] = $this->M_Order->get_order_count_product();
        $this->load->viewPurchase('purchase/order_list',$data);
    }
    
    public function orderform()
    {
        $data['nama']   = $this->session->userdata('name');
        $data['role']   = $this->session->userdata('role');
        $data['vendor'] = $this->M_Vendor->get_list_vendor();
        $this->load->viewPurchase('purchase/order_form',$data);
    }
    
    public function ordersimpan(){
        
        $data['error'] = 'Error, harap isi formulir dengan benar !';

        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        //set_rules validasi
        $this->form_validation->set_rules('id_vendor', 'Nama Vendor', 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->viewPurchase('purchase/order_form',$data);	
        }else{
           
            $this->session->set_flashdata('message', 'Berhasil menambah data... ');
            $this->M_Order->simpan_order(FALSE);
            redirect('purchase/order');
        }

    }
    
    public function orderupdatestatus(){
        
        $data['error'] = 'Error, harap isi formulir dengan benar !';

        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
       
        $status     = $this->input->post('status_order');
        $id         = $this->input->post('id_order');
        
        $this->session->set_flashdata('message', 'Berhasil mengubah data... ');
        $this->M_Order->ubah_status_order($status,$id);
        redirect('purchase/order');
        

    }
    
     public function orderdetail()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        
        $id             = $this->input->post('id_order');
        $data['order']  = $this->M_Order->get_order_id($id);
        $data['produk'] = $this->M_Produk->get_produk();
        $data['produkorder'] = $this->M_Produk_Order->get_produk_order($id);

        $this->load->viewPurchase('purchase/order_detail',$data);
    }
    
    public function produkordersimpan(){
        
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        //set_rules validasi
        $this->form_validation->set_rules('id_produk', 'Id Produk', 'trim|required');
        $this->form_validation->set_rules('id_order', 'Id Order', 'trim|required');
        $this->form_validation->set_rules('jumlah_produk', 'Jumlah Produk', 'trim|required');
        $this->form_validation->set_rules('tanggal_target_kirim', 'Tanggal Datang', 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            $data['error'] = 'Error, harap isi formulir dengan benar !';
            
            $id_order           = $this->input->post('id_order');
            $data['order']      = $this->M_Order->get_order_id($id_order);
            $data['produk']     = $this->M_Produk->get_produk();
            $data['produkorder'] = $this->M_Produk_Order->get_produk_order($id_order);
            
            $this->load->viewPurchase('purchase/order_detail',$data);
        }else{
       
            $id_order           = $this->input->post('id_order');
            $kode               = $this->input->post('id_produk');
            $id_produk          = explode(".", $kode)[0];
            $harga_produk       = explode(".", $kode)[1];
            
            
            $result = $this->M_Produk_Order->cek_order_produk($id_order,$id_produk);
            
            if($result==null){
                $this->session->set_flashdata('message', 'Berhasil menambah data... ');
                $this->M_Produk_Order->simpan_produk_order($id_produk,$harga_produk);
            }else{
                $jumlah_sebelumnya  = $result['jumlah_produk'];
                $this->session->set_flashdata('message', 'Berhasil mengubah data... ');
                $this->M_Produk_Order->update_produk_order($jumlah_sebelumnya,$id_produk,$harga_produk);
            }
            
            $data['order']      = $this->M_Order->get_order_id($id_order);
            $data['produk']     = $this->M_Produk->get_produk();
            $data['produkorder'] = $this->M_Produk_Order->get_produk_order($id_order);
            
            $this->load->viewPurchase('purchase/order_detail',$data);
            
        }
        
    }
    
    public function produkorderhapus() {
            
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');

        $id     = $this->input->post('id_order');
        $id1    = $this->input->post('id_produk_order');


        $this->M_Produk_Order->hapus_produk_order($id1);

        $data['order']          = $this->M_Order->get_order_id($id);
        $data['produk']         = $this->M_Produk->get_produk();
        $data['produkorder']    = $this->M_Produk_Order->get_produk_order($id);

        $this->session->set_flashdata('message', 'Berhasil menghapus data... ');

        $this->load->viewPurchase('purchase/order_detail',$data);
    }
    
    
     public function stok()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        
        $data['stok']    = $this->M_Pengeluaran->get_stok();
        $this->load->viewPurchase('purchase/stok_list',$data);
    }
    
    public function pembayaran()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        
        $data['pembayaran']    = $this->M_Pengeluaran->get_list_pembayaran();
        $this->load->viewPurchase('purchase/pembayaran_list',$data);
    }
    
     public function ordercetak()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');

        
        $data['order']  = $this->M_Order->get_order();
        
        $html = $this->load->view('purchase/cetak_order', $data,true);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }
    
    public function produkcetak()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');

        
        $data['produk'] = $this->M_Produk->get_produk();
        
        $html = $this->load->view('purchase/cetak_produk', $data,true);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }
    
    
    public function vendorcetak()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');

        
         $data['vendor'] = $this->M_Vendor->get_list_vendor();
        
        $html = $this->load->view('purchase/cetak_vendor', $data,true);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }
    
    public function stokcetak()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');

        
        $data['stok']    = $this->M_Pengeluaran->get_stok();
        
        $html = $this->load->view('purchase/cetak_stok', $data,true);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }
    
    public function pembayarancetak()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');

        
        $data['pembayaran']    = $this->M_Pengeluaran->get_list_pembayaran();
        
        $html = $this->load->view('purchase/cetak_pembayaran', $data,true);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }
    
    
}
