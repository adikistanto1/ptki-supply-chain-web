<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendors extends CI_Controller {
    
    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('logged_in')) {
            redirect('auth');
        } else{
           if ($this->session->userdata('role')!="Vendor") {
                redirect('auth/loginform');
           } 
        }

        //require_once __DIR__ . '/vendor/autoload.php';
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('M_User');
        $this->load->model('M_Produk');
        $this->load->model('M_Kategori_Produk');
        $this->load->model('M_Vendor');
        $this->load->model('M_Order');
        $this->load->model('M_Produk_Order');
    }


    public function index()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
         
        $this->load->viewVendor('vendors/dashboard',$data);
    }
    
    public function order()
    {
        $data['nama']           = $this->session->userdata('name');
        $data['role']           = $this->session->userdata('role');  
        $id_vendor              = $this->session->userdata('id_vendor');  
        $data['order']          = $this->M_Order->get_order_vendor($id_vendor);
        $data['produkorder']    = $this->M_Order->get_order_count_product();
        $this->load->viewVendor('vendors/order_list',$data);
    }
    
    
    public function orderupdatestatus(){
        
        $data['error'] = 'Error, harap isi formulir dengan benar !';

        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
       
        $status     = $this->input->post('status_order');
        $id         = $this->input->post('id_order');
        
        $this->session->set_flashdata('message', 'Berhasil mengubah data... ');
        $this->M_Order->ubah_status_order($status,$id);
        redirect('purchase/order');
        

    }
    
     public function orderdetail()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
        
        $id                     = $this->input->post('id_order');
        $data['order']          = $this->M_Order->get_order_id($id);
        $data['produk']         = $this->M_Produk->get_produk();
        $data['produkorder']    = $this->M_Produk_Order->get_produk_order($id);

        $this->load->viewVendor('vendors/order_detail',$data);
    }
    
    public function cetakds()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
      
        $id_order           = $this->input->post('id_order');
        $id_produk_order    = $this->input->post('id_produk_order');
        
        $data['order']          = $this->M_Order->get_order_id($id_order);
        $data['produkorder']    = $this->M_Produk_Order->get_produk_order_id($id_produk_order);
        
        $html = $this->load->view('vendors/ds_cetak', $data,true);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }
    
    public function cetaksuratjalan()
    {
        $data['nama'] = $this->session->userdata('name');
        $data['role'] = $this->session->userdata('role');
      
        $id_order           = $this->input->post('id_order');
        
        $data['order']          = $this->M_Order->get_order_id($id_order);
        $data['produkorder']    = $this->M_Produk_Order->get_produk_order($id_order);
        
        $html = $this->load->view('vendors/sj_cetak', $data,true);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }
}
